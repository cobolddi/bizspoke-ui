<?php include('template-parts/header.php'); ?>

<section class="ContactUsSection">
	<div class="container">
		<div class="row">
			<div class="col-xl-7 col-md-6 order-2-md">
				<div class="wrapper">
				<div class="mainHeading text-left">
					<h1>How can we help you today?</h1>
					<p>We are looking forward to hearing from you! Please fill out the form below, and we will get back to you at the earliest.</p>
				</div>
					<div class="formWrap">
						<form>
							<div class="input-group">
								<label for="name">Name*</label>
								<input type="text" name="name" id="name" placeholder="Enter your Name" required>
							</div>
							<div class="input-group">
								<label for="email">Email*</label>
								<input type="email" name="email" id="email" placeholder="Enter your Email" required>
							</div>
							<div class="input-group">
								<label for="phone">Phone Number*</label>
								<input type="tel" name="phone" id="phone" placeholder="Enter your Phone" required>
							</div>
							<div class="input-group">
								<label for="company_name">Company Name</label>
								<input type="text" name="company_name" id="company_name" placeholder="Company Name">
							</div>
							<div class="input-group textarea">
								<label for="message">Message</label>
								<textarea placeholder="Enter your Message" id="message"></textarea>
							</div>
							<div class="btnWrap">
								<input type="submit" value="Submit">
							</div>
						</form>
					</div>
				</div>
			</div>
            <div class=" col-xl-5 col-md-6 mb-4-md">
				<div class="contactInfo">
					<div class="innerWrap">
						<!-- <div class="b-shape-hexagon">
						</div> -->
						<div class="addressContainer">
							<div class="address">
								<span>Mumbai</span>
								<address>1603, Lodha Supremus, Senapati Bapat Road, Lower Parel</address>
							</div>
							<div class="address">
								<span>USA</span>
								<address>Bizspoke INC 136 Pacelot ST, Tryon NC 28782</address>
							</div>
						</div>
						
						<div class="contact">
							<a href="tel:+91 22 6666 8661">Tel +91 22 6666 8661</a>
							<a href="mailto:info@Bizspoke.co.in">info@bizspoke.co.in</a>
						</div>
						<div class="socialMedia" style="margin-top: 15px;">
							<a href="https://in.linkedin.com/" target="_blank"><img src="assets/img/linkedin-name.svg" alt="social_icon"></a>
						</div>
						<!-- <div class="socialIcons">
							<ul>
								<li><a href="#" target="_blank"><img src="assets/img/facebook.svg"></a></li>
								<li><a href="#" target="_blank"><img src="assets/img/instagram.svg"></a></li>
								<li><a href="#" target="_blank"><img src="assets/img/linkedin.svg"></a></li>
								<li><a href="#" target="_blank"><img src="assets/img/twitter.svg"></a></li>
							</ul>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Parallax -->
	<div class="parallax_elements">
		<div class="yellow-small-circle circle" data-relative-input="true" id="scene1">
			<div class="imgWrap" data-depth="0.6">
				<img src="assets/img/yellow-small-circle.png" alt="image">
		  </div>
		</div>
		<div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2">
			<div class="imgWrap" data-depth="0.6">
				<img src="assets/img/yellow-medium-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
		</div>
		<div class="blue-small-circle circle"  data-relative-input="true" id="scene3">
			<div class="imgWrap" data-depth="0.6">
				<img src="assets/img/small-blue-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		</div>
		<div class="blue-big-circle circle"  data-relative-input="true" id="scene4">
			<div class="imgWrap" data-depth="0.6">
				<img src="assets/img/small-blue-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		</div>
	</div>
</section>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.4139827584!2d72.82737541479838!3d19.001471087130692!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ceed3fa79e97%3A0xcc4354e040d148ef!2sLodha%20Supremus!5e0!3m2!1sen!2sin!4v1614155817974!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

<?php include('template-parts/footer.php'); ?>