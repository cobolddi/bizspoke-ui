<?php include('template-parts/header.php'); ?>


<section class="singleCaseStudyPage Section">
	<div class="SmallContainer">
    <div class="caseStudyCard">
      <div class="caseStudyCard_imageWrap">
        <img src="assets/img/tempimage/fireside-chat-series.png" alt="event_image">
      </div>
      <div class="caseStudyCard_content">
        <div class="leftlogoRightDate mb-2">
          <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
          <small class='grayText'>20 March 2020</small>
        </div>
        <h3>Fireside Chat Series</h3>
        <p>Fireside Chats were conceptualized and executed by Bizspoke to digitally connect India’s industry leaders and experts with YPO Bombay members. During the interactive sessions, a wide range of topics ― AI, parenting, the psychology of money and other topics were discussed.</p>
      </div>
    </div>
    <div class="lefContentRightImage mt-2">
      <div class="row">
        <div class="col-md-6 mb-2-md">
          <div class="para">
            <p><strong>Selected Speakers:</strong> Byju Raveendran, Maye Musk, Christopher Voss, Morgan Housel, Sanjiv Mehta, Sanjeev Bikhchandani, Dr. Mohanbir Sawhney</p>
          </div>
          <h2>KPIs across event:</h2>
          <ul class="blueList bullets">
            <li>Total number of participants: 600+ (= total smiles created)</li>
            <li>Total topics discussed: 10+</li>
            <li>Total event time:
              <ul>
                <li>Each session: 60 minutes</li>
                <li>Total: 2-3 sessions each month</li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <!-- <div class="imgWrap">
            <img src="assets/img/tempimage/member-image-756.png" alt="image">
          </div> -->
        </div>
      </div>
    </div>
	</div>
</section>

 
<?php include('template-parts/footer.php'); ?>

