<?php include('template-parts/header.php'); ?>

<?php// @include('template-parts/SingleEventPage/eventDescription.php'); ?>

<section class="eventPlanStart">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="contentWrap">
					<div class="mainHeading text-left">
						<h1>Sports Events</h1>
						<p>Bizspoke dedicates itself to its clients’ and partners’ needs in order to create unparalleled sports experiences in India. Our know-how lies in delivering a wide spectrum of services within the boundaries and opportunities of sport events based on international quality standards.</p>
					</div>
					<div class="btnWrap">
						<a href="contact-us.php" class="Btn transparent">GET STARTED</a>
					</div>
          <div class="listMain">
            <h3>Bizspoke offers holistic concepts, services and on-point-execution, such as:</h3>
            <div class="accordion">
              <div class="accordion_content">
                <div class="accordion_head blueText">IP Creation</div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Stadium Management:</div>
                <div class="accordion_body" >
                  <ul class="buletText">
                    <li>Venue Branding</li>
                    <li>Venue Operations</li>
                    <li>Sports Operations</li>
                    <li>Hospitality</li>
                  </ul>
                </div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Franchise Management</div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Broadcasting</div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Logistics</div>
              </div>
              
            </div>
          </div>
					<!-- <ul>
						<li><a href="">SPORTS EVENT ORGANISATION</a></li>
						<li><a href="">PLANNING & EXECUTION</a></li>
						<li><a href="">STAGE & CEREMONY DESIGN</a></li>
						<li><a href="">TURN KEY SPORTS EVENTS</a></li>
					</ul> -->
				</div>
			</div>
			<div class="col-lg-7">
				<div class="b-shape-hexagon">
					<img src="assets/img/tempimage/sports-image-65.png" alt="image" class="ob-fit-cover">
				</div>
			</div>
		</div>
	</div>
  <!-- Parallax -->
	<div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2">
      <div class="imgWrap" data-depth="0.9">
      <img src="assets/img/yellow-medium-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>

<?php @include('template-parts/SingleEventPage/eventsCardSection.php'); ?>
<?php @include('template-parts/SingleEventPage/imageGallery.php'); ?>


<?php include('template-parts/footer.php'); ?>