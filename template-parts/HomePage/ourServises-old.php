<section class="topSvgIconBottomContent" id="vivusAnimatedIcon">
	<div class="container">
  <h2 class="textCenter">Our Services</h2>
		<div class="row">
			<div class="col-md-3 col-6 mb-3-md">
				<a href="#">
					<!-- <svg class="icon icon-Virtual vivus_icon" id="icon-Virtual">
						<use xlink:href="assets/img/sprite.svg#icon-Virtual"></use>
					</svg> -->
					<svg  viewBox="0 0 36 32" id="icon-Virtual">
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M35.225 11.91c-0.032 1.969-0.736 3.768-1.893 5.183l0.012-0.015c-2.497-3.208-7.245-5.543-12.878-6.092 0.395-0.52 0.633-1.177 0.633-1.891 0-0.007-0-0.013-0-0.020v0.001c-0.074-1.722-1.488-3.091-3.222-3.091s-3.148 1.368-3.222 3.084l-0 0.007c0 0.002 0 0.005 0 0.007 0 0.717 0.238 1.378 0.639 1.91l-0.006-0.008c-5.633 0.56-10.381 2.884-12.878 6.092-1.149-1.399-1.858-3.197-1.892-5.161l-0-0.008c0-6.31 7.772-11.389 17.358-11.389s17.352 5.079 17.352 11.389z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M7.733 21.154v10.331c-4.373-2.066-7.218-5.437-7.218-9.239v-10.336c0.032 1.969 0.736 3.768 1.893 5.183l-0.012-0.015c1.423 1.733 3.208 3.115 5.245 4.039l0.091 0.037z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M35.225 11.91v10.336c0 3.796-2.8 7.156-7.178 9.222v-10.331c2.111-0.962 3.882-2.337 5.277-4.034l0.020-0.025c1.145-1.4 1.85-3.199 1.881-5.161l0-0.007z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M21.093 9.076c0 0.006 0 0.012 0 0.019 0 0.713-0.238 1.371-0.638 1.899l0.006-0.008c-0.592 0.792-1.527 1.299-2.581 1.299-1.775 0-3.214-1.439-3.214-3.214s1.439-3.214 3.214-3.214c1.775 0 3.214 1.439 3.214 3.214 0 0.002 0 0.004 0 0.006v-0z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M12.912 16.848h10.118"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M17.873 12.296v9.054l5.067 5"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.8399" d="M12.806 26.44l5.067-5.095"></path>
					</svg>
					<h3>Planning</h3>
					<ul>
						<li>Strategy and Consulting</li>
						<li>Research and Insights</li>
						<li>Creative Concepts</li>
					</ul>
				</a>
			</div>
			<div class="col-md-3 col-6 mb-3-md">
				<a href="#">
					<!-- <svg class="icon icon-production"><use xlink:href="assets/img/sprite.svg#icon-production"></use></svg> -->
					<svg id="icon-production" viewBox="0 0 32 32">
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M31.563 0.655v30.701c0 0.084-0.068 0.151-0.151 0.151v0h-23.679c-0.081-0.003-0.146-0.070-0.146-0.151 0-0 0-0 0-0v0-5.868h20.549c-3.83-0.274-4.289-2.973-4.345-3.724-0.006-0.11-0.096-0.196-0.207-0.196-0.002 0-0.004 0-0.006 0h-15.991v-20.913c0-0 0-0 0-0 0-0.082 0.065-0.148 0.145-0.151h23.68c0.084 0 0.151 0.068 0.151 0.151v0z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M14.642 4.048c0 0.584-0.474 1.058-1.058 1.058s-1.058-0.474-1.058-1.058c0-0.584 0.474-1.058 1.058-1.058s1.058 0.474 1.058 1.058z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M26.641 4.048c0 0.584-0.474 1.058-1.058 1.058s-1.058-0.474-1.058-1.058c0-0.584 0.474-1.058 1.058-1.058s1.058 0.474 1.058 1.058z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M22.050 18.864l-2.369-1.243c-0.029-0.015-0.062-0.023-0.098-0.023s-0.069 0.009-0.099 0.024l0.001-0.001-2.363 1.249c-0.027 0.013-0.059 0.021-0.092 0.021-0.117 0-0.213-0.095-0.213-0.213 0-0.011 0.001-0.022 0.003-0.033l-0 0.001 0.442-2.632c0.002-0.010 0.003-0.022 0.003-0.035 0-0.059-0.025-0.112-0.064-0.15l-0-0-1.915-1.831c-0.038-0.038-0.061-0.091-0.061-0.149 0-0.104 0.074-0.19 0.172-0.209l0.001-0 2.648-0.386c0.069-0.014 0.125-0.057 0.156-0.116l0.001-0.001 1.176-2.402c0.037-0.067 0.107-0.112 0.188-0.112s0.151 0.045 0.187 0.111l0.001 0.001 1.187 2.369c0.031 0.062 0.091 0.105 0.162 0.112l0.001 0 2.643 0.381c0.102 0.017 0.178 0.105 0.178 0.21 0 0.058-0.023 0.11-0.060 0.149l0-0-1.909 1.87c-0.040 0.038-0.064 0.091-0.064 0.15 0 0.012 0.001 0.024 0.003 0.036l-0-0.001 0.459 2.632c0.002 0.010 0.002 0.021 0.002 0.032 0 0.117-0.095 0.213-0.213 0.213-0.033 0-0.065-0.008-0.093-0.021l0.001 0.001z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M28.136 25.488h-24.139s-2.912-0.179-3.488-3.919h23.069c0.002-0 0.004-0 0.006-0 0.11 0 0.201 0.087 0.207 0.196l0 0.001c0.056 0.75 0.515 3.432 4.345 3.724z"></path>
						<path fill="none" stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M31.563 22.705s-1.299 2.872-3.427 2.8"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8399" d="M7.419 7.525h24.144"></path>
					</svg>
					<h3>Production</h3>
					<ul>
						<li>Project Management   </li>
						<li>Venue Operations</li>
						<li>Content & Video</li>
						<li>Manpower & Logistics</li>
					</ul>
				</a>
			</div>
			<div class="col-md-3 col-6 mb-3-md">
				<a href="#">
					<!-- <img src="assets/img/virtual.svg" alt="image"> -->
					<!-- <svg class="icon icon-palnning"><use xlink:href="assets/img/sprite.svg#icon-palnning"></use></svg> -->
					<svg id="icon-palnning" viewBox="0 0 32 32">
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8238" d="M0.621 13.553h30.731c0.103 0 0.187 0.084 0.187 0.187v17.667c0 0.103-0.084 0.187-0.187 0.187h-30.731c-0.103 0-0.187-0.084-0.187-0.187v-17.667c0-0.103 0.084-0.187 0.187-0.187z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8238" d="M22.461 24.092c-0.18 0.755-0.464 1.42-0.84 2.022l0.016-0.028c-0.019 0.023-0.048 0.037-0.080 0.037s-0.060-0.014-0.079-0.037l-0-0c-0.232-0.179-0.526-0.287-0.846-0.287-0.050 0-0.1 0.003-0.148 0.008l0.006-0.001c-0.644 0.081-1.149 0.586-1.229 1.223l-0.001 0.007c-0.005 0.043-0.007 0.092-0.007 0.142 0 0.32 0.108 0.615 0.29 0.849l-0.002-0.003c0.023 0.019 0.037 0.048 0.037 0.080s-0.014 0.060-0.037 0.079l-0 0c-0.573 0.36-1.238 0.644-1.948 0.814l-0.046 0.009c-0.007 0.002-0.015 0.003-0.024 0.003-0.054 0-0.099-0.039-0.108-0.090l-0-0.001c-0.107-0.67-0.681-1.176-1.373-1.176s-1.266 0.506-1.372 1.168l-0.001 0.008c-0.010 0.052-0.054 0.090-0.108 0.090-0.008 0-0.017-0.001-0.024-0.003l0.001 0c-0.755-0.18-1.42-0.464-2.022-0.84l0.028 0.016c-0.023-0.019-0.037-0.048-0.037-0.080s0.014-0.060 0.037-0.079l0-0c0.179-0.232 0.287-0.526 0.287-0.846 0-0.050-0.003-0.1-0.008-0.148l0.0010.006c-0.085-0.648-0.6-1.154-1.246-1.224l-0.006-0.001c-0.043-0.005-0.092-0.007-0.142-0.007-0.32 0-0.615 0.108-0.849 0.29l0.003-0.002c-0.019 0.023-0.048 0.037-0.080 0.037s-0.060-0.014-0.079-0.037l-0-0c-0.365-0.581-0.651-1.256-0.82-1.976l-0.009-0.045c-0.002-0.007-0.003-0.015-0.003-0.024 0-0.054 0.039-0.099 0.090-0.108l0.001-0c0.644-0.127 1.124-0.687 1.124-1.359s-0.479-1.233-1.115-1.358l-0.009-0.001c-0.052-0.010-0.090-0.054-0.090-0.108 0-0.008 0.001-0.017 0.003-0.024l-0 0.001c0.191-0.748 0.479-1.405 0.857-1.999l-0.017 0.028c0.019-0.023 0.048-0.037 0.080-0.037s0.060 0.014 0.079 0.037l0 0c0.23 0.176 0.523 0.283 0.84 0.283 0.544 0 1.015-0.313 1.243-0.769l0.004-0.008c0.087-0.176 0.138-0.383 0.138-0.602 0-0.318-0.108-0.612-0.288-0.846l0.002 0.003c-0.023-0.019-0.037-0.048-0.037-0.080s0.014-0.060 0.037-0.079l0-0c0.573-0.365 1.239-0.649 1.95-0.815l0.044-0.009c0.007-0.002 0.015-0.002 0.023-0.002 0.056 0 0.102 0.041 0.109 0.095l0 0.001c0.089 0.686 0.67 1.21 1.373 1.21s1.284-0.524 1.372-1.203l0.001-0.007c0.007-0.054 0.053-0.096 0.109-0.096 0.008 0 0.016 0.001 0.024 0.003l-0.001-0c0.751 0.178 1.413 0.464 2.008 0.845l-0.026-0.015c0.023 0.019 0.037 0.048 0.037 0.080s-0.014 0.060-0.037 0.079l-0 0c-0.175 0.229-0.28 0.519-0.28 0.833 0 0.69 0.507 1.261 1.168 1.362l0.008 0.001c0.062 0.010 0.134 0.016 0.208 0.016 0.316 0 0.607-0.107 0.839-0.287l-0.003 0.002c0.019-0.023 0.048-0.037 0.080-0.037s0.060 0.014 0.079 0.037l0 0c0.36 0.573 0.644 1.238 0.814 1.948l0.009 0.046c0.002 0.007 0.003 0.015 0.003 0.024 0 0.054-0.039 0.099-0.090 0.108l-0.001 0c-0.686 0.089-1.21 0.67-1.21 1.373s0.524 1.284 1.203 1.372l0.007 0.001c0.061 0 0.11 0.049 0.11 0.11v0z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.8238" d="M18.76 22.604c0 1.51-1.224 2.735-2.735 2.735s-2.735-1.224-2.735-2.735c0-1.51 1.224-2.735 2.735-2.735s2.735 1.224 2.735 2.735z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8238" d="M3.031 4.229v0.884c0 0.786 0.637 1.422 1.422 1.422v0h0.45v7.018h-3.767v-12.082c0.006-0.544 0.449-0.983 0.994-0.983 0.002 0 0.004 0 0.006 0h1.768c0.552 0 1 0.447 1 1v0 1.318h-0.45c-0.786 0-1.422 0.637-1.422 1.422v0z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8238" d="M30.869 1.488v12.082h-3.767v-7.035h0.461c0.786 0 1.422-0.637 1.422-1.422v0-0.884c0-0.786-0.637-1.422-1.422-1.422v0h-0.461v-1.318c0-0.002 0-0.004 0-0.006 0-0.549 0.445-0.994 0.994-0.994 0.002 0 0.004 0 0.006 0h1.768c0.552 0 1 0.447 1 1v0z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="10" stroke-width="0.8238" d="M4.454 2.806h23.109c0.786 0 1.422 0.637 1.422 1.422v0.884c0 0.786-0.637 1.422-1.422 1.422h-23.109c-0.786 0-1.422-0.637-1.422-1.422v-0.884c0-0.786 0.637-1.422 1.422-1.422z"></path>
					</svg>
					<h3>Virtual</h3>
					<ul>
						<li>Virtual Events</li>
						<li>Virtual Reality</li>
						<li>Augmented Reality</li>
						<li>Immersive Web Platforms</li>
					</ul>
				</a>
			</div>
			<div class="col-md-3 col-6 mb-3-md">
				<a href="#">
					<!-- <img src="assets/img/entertainment.svg" alt="image"> -->	
					<!-- <svg class="icon icon-entertainment" id="icon_entertainment"><use xlink:href="assets/img/sprite.svg#icon-entertainment"></use>
					</svg> -->
					<svg id="icon-entertainment" viewBox="0 0 38 32">
						<path  stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M9.241 4.224c-0.687 0.154-1.476 0.243-2.286 0.243-1.208 0-2.371-0.197-3.457-0.561l0.077 0.022s-1.6-0.76-2.302-0.449-0.898 1.088-0.813 2.772 0.422 7.145 1.985 10.841c0.729 1.771 1.684 3.296 2.852 4.629l-0.016-0.019c1.815 2.013 4.337 3.357 7.172 3.64l0.046 0.004c0.153 0 0.301 0 0.444 0 0.013 0 0.028 0 0.043 0 0.327 0 0.64-0.056 0.932-0.16l-0.020 0.006c1.442-0.496 5.766-2.508 6.558-9.141 0 0 1.215-6.5-2.725-15.002-0.115-0.385-0.466-0.66-0.882-0.66-0.243 0-0.465 0.095-0.629 0.249l0-0c-1.056 0.713-2.059 2.778-6.168 3.411-0.275 0.048-0.549 0.095-0.813 0.174z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M12.267 10.978s-0.026-2.999 2.698-2.529c0 0 1.262 0.275 1.732 0.877 0 0-1.056 1.875-2.614 2.149s-1.816-0.496-1.816-0.496z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M9.341 11.411s-1.352-2.677-3.533-1.009c0 0-0.993 0.824-1.13 1.584 0 0 1.816 1.167 3.306 0.702s1.357-1.278 1.357-1.278z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M7.182 18.281s5.756 1.964 9.288-1.912c0 0-0.554 4.166-3.696 4.721-0.011-0.021-2.556 1.088-5.592-2.809z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M23.002 8.396s5.999 4.589 11.744 2.112c0 0 2.471-0.982 2.292 1.526s-0.739 8.877-2.767 12.673-5.090 6.785-9.378 6.933-6.764-5.603-6.764-5.603"></path>
						<path fill="none" stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M29.661 26.44s-4.055-3.987-8.602-2.213c0 0 2.144-3.221 5.027-2.387 0 0 2.582 0.132 3.575 4.599z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M28.874 17.991s1.178-2.603 3.337-1.093c0 0 0.982 0.745 1.141 1.452 0 0-1.679 1.188-3.116 0.803s-1.362-1.162-1.362-1.162z"></path>
						<path stroke="#0474b5" style="stroke: var(--color1, #0474b5)" stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="4" stroke-width="0.7921" d="M26.17 17.188s-0.095-2.851-2.667-2.297c0 0-1.188 0.312-1.584 0.908 0 0 1.098 1.737 2.572 1.933s1.679-0.544 1.679-0.544z"></path>
					</svg>
					<h3>Entertainment</h3>
					<ul>
						<li>Artist Management </li>
						<li>Talent Management</li>
						<li>Hospitality</li>
					</ul>
				</a>
			</div>
		</div>
	</div>
</section>