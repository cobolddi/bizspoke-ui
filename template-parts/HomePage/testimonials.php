<section class="testimonials pseduoElement">
  <div class="container">
    <h2 class="mb-0">Testimonial</h2>
    <div class="testimonialSlider">
      <div class="testimonialSlide">
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id risus lectus. Duis sodales tristique mauris rhoncus posuere. Sed ac bibendum erat. Aliquam tempor mauris eu sapien tincidunt, nec tristique justo tincidunt. Proin bibendum libero fringilla lacinia faucibus.</p>
        </div>
        <div class="profile">
          <img src="assets/img/testimonial_image.png" alt="testimonial_image">
          <h3>Puneet Anand</h3>
          <span>company Name</span>
        </div>
      </div>
      <!-- <div class="testimonialSlide">
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id risus lectus. Duis sodales tristique mauris rhoncus posuere. Sed ac bibendum erat. Aliquam tempor mauris eu sapien tincidunt, nec tristique justo tincidunt. Proin bibendum libero fringilla lacinia faucibus.</p>
        </div>
        <div class="profile">
          <img src="assets/img/testimonial_image.png" alt="testimonial_image">
          <h3>Puneet Anand</h3>
          <span>company Name</span>
        </div>
      </div>
      <div class="testimonialSlide">
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id risus lectus. Duis sodales tristique mauris rhoncus posuere. Sed ac bibendum erat. Aliquam tempor mauris eu sapien tincidunt, nec tristique justo tincidunt. Proin bibendum libero fringilla lacinia faucibus.</p>
        </div>
        <div class="profile">
          <img src="assets/img/testimonial_image.png" alt="testimonial_image">
          <h3>Puneet Anand</h3>
          <span>company Name</span>
        </div>
      </div> -->
    </div>
  </div>
  <div class="parallax_elements">
      <div class="yellow-small-circle circle" data-relative-input="true" id="scene1b">
        <div class="imgWrap" data-depth="0.6">
        <img src="assets/img/yellow-small-circle.png" alt="image">
          </div>
      </div>
      <div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2b">
        <div class="imgWrap" data-depth="0.6">
        <img src="assets/img/yellow-medium-circle.png" alt="image">
          </div>
        <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
      </div>
      <div class="blue-small-circle circle"  data-relative-input="true" id="scene3b">
        <div class="imgWrap" data-depth="0.6">
        <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
          </div>
        <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
      </div>
    </div>
</section>