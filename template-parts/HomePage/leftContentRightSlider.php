<section class="leftContentRightSlider">
	<div class="container">
		<div class="row alignItemCenter">
			<div class="col-lg-5 order-2-lg title_animation">
				<h1 class="title-Wrap">
				<span class="inner_title">Architects of exceptional experiences</span>
				<!-- <span class="inner_title"> </span> -->
			    </h1>
				<p class="animated-para">Bizspoke is a company that curates and creates immersive audience engagement platforms. Our core objective is to offer clients the opportunity to engage with their audiences.</p>
				<!-- <svg width="100%" height="100%" viewBox="30 -50" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
				 <path id="path">
						<animate attributeName="d" from="m0,110 h0" to="m0,110 h1100" dur="6.8s" begin="0s" repeatCount="indefinite"/>
					</path>
					<text font-size="26" font-family="Montserrat" fill='hsla(36, 95%, 85%, 1)'>
						<textPath xlink:href="#path">I'm An Animated Typing Example && I'm All SVG.
				    </textPath>
					</text>
				</svg> -->
				<a href="about-us.php" class="Btn transparent anim_btn">EXPLORE NOW</a>
			</div>
			<div class="col-lg-7 mb-8-lg px-0-sm">
				<div class="hexagonImageWrap">
					<div class="hexagonSlider">
					<div class="hexagonImage">
              <img src="assets/img/tempimage/home-page-banner1.png" alt="hexa_image">
            </div>
            <!-- <div class="hexagonImage">
              <img src="assets/img/events-images/hexa_image.png" alt="event_image">
            </div>
            <div class="hexagonImage">
              <img src="assets/img/events-images/bizspoke-intro.jpg" alt="event_image">
            </div> -->
          </div>
				</div>
			</div>
		</div>
	</div>
</section>