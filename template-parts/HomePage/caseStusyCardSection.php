<section class="caseStudyCardSection Section">
	<div class="container">
		<div class="row justifyCenter">
			<!-- Card 1 -->
			<div class="col-md-4 col-sm-6 mb-3-md">
				<a href="single-case-study.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/iifl-image-screenshot.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>IIFL WAM Surge</h3>
						<p>During the midst of the lockdown, IIFL decided to host a fun evening to motivate its team member</p>
					</div>
				</a>
			</div>
			<!-- Card 2 -->
			<div class="col-md-4 col-sm-6 mb-3-md">
				<a href="chapter-opening-ceremony.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/reimond-de-zuniga.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<!-- <img src="assets/img/tempimage/iifl-logo-small.png" alt="logo"> -->
							<span class="text">Corporate Client</span>
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>Chapter Opening Ceremony</h3>
						<p>To welcome its new members and bid farewell to outgoing associates, Corporate Client…</p>
					</div>
				</a>
			</div>
			<!-- Card 3 -->
			<div class="col-md-4 col-sm-6 mb-3-md">
				<a href="iifl-wealth-masterclass-with-gary-mehigan.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/iifl-image-4c.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>IIFL Wealth Masterclass with Gary Mehigan</h3>
						<p>Aiming to offer its clients a one-of-a-kind-experience, IIFL Wealth tasked Bizspoke to create a hybrid event…</p>
					</div>
				</a>
			</div>
		</div>
		<div class="btnWrap text-center">
			<a href="case-studies.php" class="Btn transparent">SEE ALL PROJECTS</a>
		</div>
	</div>
</section>