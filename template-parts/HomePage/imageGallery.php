
<section class="gallery">
 	<div class="container">
 		<div class="row zoom-gallery">
 			<div class="col-md-6 col-12">
 				<div class="ImgWrap">
 					<a href="assets/img/gallery-images/gallery-image1.png">
 					<img src="assets/img/gallery-images/gallery-image1.png" alt="image">
 					</a>
 				</div>
 			</div>
 			<div class="col-md-3 col-6">
				<div class="img-group">
					<div class="ImgWrap">
						<a href="assets/img/gallery-images/gallery-image2.png">
							<img src="assets/img/gallery-images/gallery-image2.png" alt="image">
						</a>
					</div>
					<div class="ImgWrap">
						<a href="assets/img/gallery-images/gallery-image3.png">
						<img src="assets/img/gallery-images/gallery-image3.png" alt="image">
						</a>
					</div>
				</div>
 			</div>
 			<div class="col-md-3 col-6">
 				<div class="ImgWrap">
 					<a href="assets/img/gallery-images/gallery-image4.png">
 						<img src="assets/img/gallery-images/gallery-image4.png" alt="image">
 					</a>
 				</div>
 			</div>
 		</div>

 		<div class="btnWrap text-center">
 			<a href="gallery-page.php" class="Btn transparent">See More</a>
 		</div>
 	</div>
</section>