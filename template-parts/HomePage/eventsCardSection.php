<section class="topImageBottomContent pb-4">
	<div class="container">
		<div class="row justifyCenter eventsAnimated">
			<div class="col-md-4 col-sm-6 mb-3-md">
				<a href="corporate-events.php" class="EventCardWrap">
					<div class="imgWrap mb-2">
						<img src="assets/img/events-images/corporate-event-76.png" alt="event_image">
					</div>
          <div class="content animContent">
            <h3>Corporate Events</h3>
            <ul class="anim_ul">
              <li class="anim_li">Employee Engagement – Team Building Events </li>
              <li class="anim_li">Annual Day / Family Day</li>
              <li class="anim_li">Rewards & Recognitions Night</li>
            </ul>
          </div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mb-3-md">
				<a href="celebrations.php" class="EventCardWrap">
					<div class="imgWrap mb-2">
						<img src="assets/img/events-images/celebration-event-76.png" alt="event_image">
					</div>
          <div class="content animContent">
            <h3>Celebrations</h3>
            <ul class="anim_ul">
              <li class="anim_li">Anniversary / Birthday</li>
              <li class="anim_li">Cocktail Parties</li>
              <li class="anim_li">Weddings</li>
            </ul>
          </div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mb-3-md">
				<a href="sports-events.php" class="EventCardWrap">
					<div class="imgWrap mb-2">
						<img src="assets/img/events-images/sports-event-76.png" alt="event_image">
					</div>
          <div class="content animContent">
            <h3>Sports Events</h3>
            <ul class="anim_ul">
              <li class="anim_li">League Development</li>
              <li class="anim_li">Venue Production / Stadium Management</li>
              <li class="anim_li">Franchise Management</li>
            </ul>
          </div>
				</a>
			</div>
		</div>
	</div>
</section>