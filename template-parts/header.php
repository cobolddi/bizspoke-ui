<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Make your vehicle last longer">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">
	<title>Bizspoke</title>
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->

</head>
<body id="body" class="body loading">
	<header id="header">
        <div class="container">
            <div class="innerHeader">
                <div class="logo">
                    <a href="<?php echo 'index.php'; ?>">
                    <img src="assets/img/main-logo.svg" alt="logo">
                    </a>
                </div>
                <div class="headerRight">
                    <a href="contact-us.php" class="Btn">Contact us</a>
                    <div class="menuBar">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menuWrapper">
            <div class="container">
                <div class="innerMenu">
                    <div class="navigationWrap">
                        <div class="navbar">
                            <nav>
                                <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li><a href="why-choose-bizspoke.php">Why choose us</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li class="hasDropdown">
                                    <a href="">Events</a>
                                    <ul>
                                        <li><a href="corporate-events.php">Corporate</a></li>
                                        <li><a href="celebrations.php">Celebrations</a></li>
                                        <li><a href="sports-events.php">Sports</a></li>
                                    </ul>
                                </li>
                                <li><a href="case-studies.php">Case studies</a></li>
                                <li><a href="gallery-page.php">Gallery</a></li>
                                <li><a href="contact-us.php">Contact us</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="getIntouch">
                            <!-- <span>To get started, simply call us on</span> <a href="tel: +91-9871069987"> +91-9871069987</a> -->
                            <span>To get started, simply call us on</span> <a href="tel: +91-2266668661"> +91-2266668661</a>
                        </div>
                    </div>
                    <div class="menuImage">
                        <div class="b-shape-hexagon">
                        <img src="assets/img/menu_image22.png" alt="image" class="ob-fit-cover">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="Loader0"></div>
    <div id="Loader1"></div>
	<main id="main">
       <div class="wrapper">