</div>
</main>
    <footer>
        <div class="container">
        	<div class="footerWrap">
        		<a href="index.php" class="footer-logo">
        		<img src="assets/img/bizspoke-text-logo.svg" alt="logo">
        		</a>
        		<nav>
	        		<ul>
	        			<li><a href="index.php">Home</a></li>
								<li><a href="about-us.php">About us</a></li>
								<li><a href="why-choose-bizspoke.php">Why choose Bizspoke</a></li>
								<li><a href="services.php">Services</a></li>
								<!-- <li><a href="single-event.php">Events We Plan</a></li> -->
								<!-- <li><a href="case-studies.php">Our work</a></li> -->
								<li><a href="gallery-page.php">Gallery</a></li>
								<li><a href="contact-us.php">Contact</a></li>
	        		</ul>
	        	</nav>
        		<div class="address">
        			<address><strong>Mumbai </strong>  1603, Lodha Supremus, Senapati Bapat Road, Lower Parel</address>
        		</div>
        		<div class="contact">
        			<ul>
        				<li>
	        				<span>Tel</span>
	        			    <a href="tel:+91 2266668661">+91 22 6666 8661</a>
        			    </li>
        			    <li>
	        			    <a href="mailto:info@Bizspoke.co.in">info@Bizspoke.co.in</a>
        			    </li>
        			</ul>
        		</div>
						<div class="socialMedia">
							<a href="https://in.linkedin.com/" target="_blank"><img src="assets/img/linkedin-name.svg" alt="social_icon"></a>
						</div>
        		<!-- <div class="social">
        			<ul>
        				<li><a href="#"><img src="assets/img/facebook.svg" alt="social_icon"></a></li>
        				<li><a href="#"><img src="assets/img/instagram.svg" alt="social_icon"></a></li>
        				<li><a href="#"><img src="assets/img/linkedin-name.svg" alt="social_icon"></a></li>
        				<li><a href="#"><img src="assets/img/twitter.svg" alt="social_icon"></a></li>
        			</ul>
        		</div> -->
        	</div>
        </div>
        <div class="footerBottom">
    		<div class="container">
    		  <span>Copyright 2021, Bizspoke</span>
    	    </div>
    	</div>
    </footer>

    <!--/footer-->
</div>
    <script src="assets/js/vendor.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vivus/0.4.5/vivus.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vivus/0.4.5/vivus.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js" integrity="sha512-/6TZODGjYL7M8qb7P6SflJB/nTGE79ed1RfJk3dfm/Ib6JwCT4+tOfrrseEHhxkIhwG8jCl+io6eaiWLS/UX1w==" crossorigin="anonymous"></script> -->
    <script src="assets/js/scripts.js"></script>
</body>
</html>