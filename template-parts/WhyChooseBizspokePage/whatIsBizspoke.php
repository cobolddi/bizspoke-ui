<section class="vision_mission Section whatIsBizspoke">
  <div class="hexagonImageMain">
    <img src="assets/img/tempimage/stem-list-54.png" alt="hexa_image">
  </div>
  <div class="container">
    <h2 class="textLeft">What is Bizspoke?</h2>
    <div class="row">
      <div class="col-md-9">
~       <ul class="whatIsBizspoke_listWrap">
          <li class="whatIsBizspoke_list">
            <div class="content">
              <img class="icon" src="assets/img/icons/wib-1.svg" alt="icon">
              <p>An authentic brand, with a big heart and good sense of humour</p>
            </div>
          </li>
          <li class="whatIsBizspoke_list">
            <div class="content">
              <img class="icon" src="assets/img/icons/wib-2.svg" alt="icon">
              <p>A set-up that follows a design thinking- and user-centric approach</p>
            </div>
          </li>
          <li class="whatIsBizspoke_list">
            <div class="content">
              <img class="icon" src="assets/img/icons/wib-3.svg" alt="icon">
              <p>A company that always goes the extra mile</p>
            </div>
          </li>
          <li class="whatIsBizspoke_list">
            <div class="content">
              <img class="icon" src="assets/img/icons/wib-3.svg" alt="icon">
              <p>A trustworthy firm with no hidden costs, and a focus on transparency throughout all stages of the project</p>
            </div>
          </li>
          <li class="whatIsBizspoke_list">
            <div class="content">
              <img class="icon" src="assets/img/icons/wib-3.svg" alt="icon">
              <p>A bold mindset that pushes limits, and challenges the firm’s team members & clients to reach beyond</p>
            </div>
          </li>
          <li class="whatIsBizspoke_list">
            <div class="content">
              <img class="icon" src="assets/img/icons/wib-3.svg" alt="icon">
              <p>A team that is always on the lookout to save the client’s money, and solves all problems along the way</p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
  <!-- Parallax Image -->
  <div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1b">
        <img src="assets/img/yellow-small-circle.png" alt="image">
      </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2b">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/yellow-medium-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle" id="scene3b">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/small-blue-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
    <div class="blue-big-circle circle" id="scene4b">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/small-blue-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>