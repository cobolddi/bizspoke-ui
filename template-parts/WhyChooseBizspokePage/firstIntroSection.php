<section class="bizspokeIntro">
	<div class="background-light-hexa"></div>
	<div class="background-light-hexa2"></div>
	<div class="what-is-bizspoke">
		<div class="SmallContainer">
			<div class="row">
				<div class="col-md-6 mb-3-md">
					<div class="hexagonImageWrap">
						<div class="hexagonImage">
						<img src="assets/img/tempimage/bizspoke-inspire-54.png" alt="hexa_image">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h2>Bizspoke inspires!</h2>
          <div class="para">
            <p>We connect with our customers, team members, and partners through trust, empathy, and creative curiosity. Our biggest strength, however, lies in in our reliability. The moment we take on a project, we manage it with professionalism, a positive attitude, and resilience to the very end! </p>
            <p>The Bizspoke team operates with attention to detail, high interpersonal and technological capability, and a quick turn-around time. We care for your event as much as you do – and this unshakable belief is not only part of our company’s DNA but also the reason why we chose our tagline: We’ve got this!</p>
          </div>
				</div>
			</div>
		</div>
	</div>
	<!-- Parallax -->
	<div class="parallax_elements">
		<div class="yellow-small-circle circle" data-relative-input="true" id="scene1">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/yellow-small-circle.png" alt="image">
		  </div>
		</div>
		<div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/yellow-medium-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
		</div>
		<div class="blue-small-circle circle"  data-relative-input="true" id="scene3">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/small-blue-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		</div>
		<div class="blue-big-circle circle"  data-relative-input="true" id="scene4">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/small-blue-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		</div>
	</div>
</section>