<section class="bizspokeUspSection blueBackground">
  <div class="container">
    <h2 class="textCenter">Bizspoke’s USPs</h2>
    <div class="row">
      <div class="col-md-4 mb-md-3">
        <div class="uspCard">
          <img src="assets/img/icons/white-gear.svg" alt="icon">
          <h3>Expertise</h3>
          <p>Bizspoke offers top-notch event proficiency due to many years of executional experience, access to A-class talent (from India as well as abroad), a deep understanding of technology, and in-depth know-how about event set-up, budgeting, and implementation</p>
        </div>
      </div>
      <div class="col-md-4 mb-md-3">
        <div class="uspCard">
          <img src="assets/img/icons/white-efficiency.svg" alt="icon">
          <h3>Efficiency</h3>
          <p>Bizspoke conceptualizes, implements, and manages events with a fast turn-around time, steady focus, and a great sense of joy from each project, start to finish</p>
        </div>
      </div>
      <div class="col-md-4 mb-md-3">
        <div class="uspCard">
          <img src="assets/img/icons/white-xpressive.svg" alt="icon">
          <h3>Expressiveness</h3>
          <p>Bizspoke differentiates itself through its creative thought-leadership, out-of-the-box ideation process, and a professional understanding of brand building, resulting in impeccable conceptual strength for its clients</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Parallax Image -->
  <div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1c">
        <img src="assets/img/yellow-small-circle.png" alt="image">
      </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2c">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/yellow-medium-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle" id="scene3c">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/light-white-small.png" alt="image">
      </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
    <div class="blue-big-circle circle" id="scene4c">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/light-white-small.png" alt="image">
      </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>