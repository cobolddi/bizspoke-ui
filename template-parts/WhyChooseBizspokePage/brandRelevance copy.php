<section class="brandRelevance">
	<div class="container">
		<div class="topHeading">
			<h2>Brand Relevance</h2>
			<p>What makes Bizspoke a good brand?</p>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="keyPoints">
					<ul>
						<li>
							<div class="counter">
							  <div class="hexagonImage"></div>
						    </div>
							<div class="pointsContent">
								<h3>Be Different</h3>
								<p>They own their story and make sure its only theirs</p>
							</div>
						</li>
						<li>
							<div class="counter">
							  <div class="hexagonImage"></div>
						    </div>
							<div class="pointsContent">
								<h3>Be Vigilant</h3>
								<p>They stay on top of the ever-changing nature of the market</p>
							</div>
						</li>
						<li>
							<div class="counter">
							  <div class="hexagonImage"></div>
						    </div>
							<div class="pointsContent">
								<h3>Be Relevant</h3>
								<p>They make sure their story resonates with its audience and is always on point</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="hexagonWrap">
					<div class="hexagonImage">
						<span>The Bizspoke Focus: </span>
						<span>Creating Value </span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="valuePoints">
					<h4>“The James Bond of Events”</h4>
					<ul>
						<li>
							<span>Problem Solving:</span> Bizspoke’s solutions are of a strategic nature, helping their clients to solve problems with creativity and innovation while achieving KPIs
						</li>
						<li>
							<span>Value for Money:</span> For a business class ticket price, Bizspoke gives its clients a first class experience
						</li>
						<li>
							<span>Innovating within Constraints:</span> Bizspoke is able to turn-around unique ideas quickly, without overstepping budget boundaries
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>