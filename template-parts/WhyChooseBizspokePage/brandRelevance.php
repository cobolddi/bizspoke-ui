<section class="brandRelevance">
	<div class="container">
		<div class="topHeading">
			<h2>Brand Relevance</h2>
			<p>What makes Bizspoke a good brand?</p>
		</div>
    <div class="spider2Container">
      <div class="spider2Container_leg">
        <div class="keyPoints">
					<ul>
						<li>
							<span class="circle"></span>
							<span class="points_bullet">1</span>
							<div class="pointsContent">
								<h3>Be Different</h3>
								<p>They own their story and make sure its only theirs</p>
							</div>
						</li>
						<li>
							<span class="circle"></span>
							<span class="points_bullet">2</span>
							<div class="pointsContent">
								<h3>Be Vigilant</h3>
								<p>They stay on top of the ever-changing nature of the market</p>
							</div>
						</li>
						<li>
							<span class="circle"></span>
							<span class="points_bullet">3</span>
							<div class="pointsContent">
								<h3>Be Relevant</h3>
								<p>They make sure their story resonates with its audience and is always on point</p>
							</div>
						</li>
					</ul>
				</div>
      </div>
      <div class="spider2Container_body">
        <div class="hexagonWrap">
          <div class="circleImage">
            <span>The Bizspoke Focus:</span>
            <h2>Creating Value</h2>
          </div>
				</div>
      </div>
      <div class="spider2Container_leg">
        <div class="valuePoints">
					<h4>"The James Bond of Events"</h4>
					<ul>
						<li>
							<span class="circle"></span>
							<span>Problem Solving:</span> Bizspoke’s solutions are of a strategic nature, helping their clients to solve problems with creativity and innovation while achieving KPIs
						</li>
						<li>
							<span class="circle"></span>
							<span>Value for Money:</span> For a business class ticket price, Bizspoke gives its clients a first class experience
						</li>
						<li>
						<span class="circle"></span>	
							<span>Innovating within Constraints:</span> Bizspoke is able to turn-around unique ideas quickly, without overstepping budget boundaries
						</li>
					</ul>
				</div>
      </div>
    </div>
	</div>
</section>