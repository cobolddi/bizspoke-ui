<div class="hexagonCheckPoints">
			<div class="SmallContainer">
				<div class="row">
					<div class="col-md-4">
						<div class="hexagonImage">
							<div class="content">
								<div class="check_circle">
									<span class="check"></span>
								</div>
								<p>An authentic brand, with a big heart and good sense of humour</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="hexagonImage">
							<div class="content">
								<div class="check_circle">
									<span class="check"></span>
								</div>
								<p>A set-up that follows a design thinking- and user-centric approach</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="hexagonImage">
							<div class="content">
								<div class="check_circle">
									<span class="check"></span>
								</div>
								<p>A company that always goes the extra mile</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="hexagonImage">
							<div class="content">
								<div class="check_circle">
									<span class="check"></span>
								</div>
								<p>A trustworthy firm with no hidden costs, and a focus on transparency throughout all stages of the project</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="hexagonImage">
							<div class="content">
								<div class="check_circle">
									<span class="check"></span>
								</div>
								<p>A bold mindset that pushes limits, and challenges the firm’s team members & clients to reach beyond</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="hexagonImage">
							<div class="content">
								<div class="check_circle">
									<span class="check"></span>
								</div>
								<p>A team that is always on the lookout to save the client’s money, and solves all problems along the way</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Parallax -->
			<div class="parallax_elements">
		    <div class="yellow-small-circle circle">
		      <div class="imgWrap" data-depth="0.9" id="scene1">
		      <img src="assets/img/yellow-small-circle.png" alt="image">
		        </div>
		    </div>
		    <div class="yellow-medium-circle circle" id="scene2">
		      <div class="imgWrap" data-depth="0.9">
		      <img src="assets/img/yellow-medium-circle.png" alt="image">
		        </div>
		      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
		    </div>
		    <div class="blue-small-circle circle" id="scene3">
		      <div class="imgWrap" data-depth="0.9">
		      <img src="assets/img/small-blue-circle.png" alt="image">
		        </div>
		      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		    </div>
		  </div>
		</div>