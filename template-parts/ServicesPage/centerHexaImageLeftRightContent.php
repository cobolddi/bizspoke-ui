<section class="ourStrength Section servicePage">
	<div class="container">
    <div class="mainHeading">
      <h1>Together, we thrive.</h1>
      <p>Bizspoke constantly innovates itself and the way its team thinks. It creates and executes events in order to help its clients grow their business, and be seen and respected by their respective audiences. Bizspoke achieves this through:</p>
    </div>
    <div class="spiderSection">
      <div class="InnerBox">
        <div class="spider">
          <ul class="spider_leg leftLeg">
            <li>
              <img class="icon" src="assets/img/icons/training-565.svg" alt="Icon"> 
              <div class="content">
                <h4>Training:</h4>
                <p>Regular workshops to stay on top of the latest trends and insights of the international and Indian event industry</p>
              </div>
            </li>
            <li>
              <img class="icon" src="assets/img/icons/technology-565.svg" alt="Icon"> 
              <div class="content">
                <h4>Technology:</h4>
                <p>Frequent upgrades and dedicated research of new tech-based solutions that increase event effectiveness, and offer new, exciting delivery mechanisms and formats</p>
              </div>
            </li>
          </ul>
          <div class="spider_body">
            <img src="assets/img/hexa-stem-list.png" alt="b-image">
          </div>
          <ul class="spider_leg rightLeg">
            <li>
              <img class="icon" src="assets/img/icons/team-565.svg" alt="Icon"> 
              <div class="content">
                <h4>Team-building:</h4>
                <p>Recurring internal team-meetings and offsites that build trust, create synergies between departments, and spark new ideas</p>
              </div>
            </li>
            <li>
              <img class="icon" src="assets/img/icons/team-565.svg" alt="Icon"> 
              <div class="content">
                <h4>Third Parties:</h4>
                <p>Strong, diverse, and transparent vendor relationships that support seamless execution and artistic variety</p>
              </div>
            </li>
          </ul>
        </div>
        
      </div>
    </div>
    <div class="headContent smallHeadContent">
			<h2>Grow with us!</h2>
			<p class="bigFont blueText">Bizspoke develops long-term partnerships with its clients as it can service adapting to different event requirements while staying consistent with quality, brand recall, and pricing</p>
		</div>
	</div>
  <!-- Parallax Element -->
  <div class="parallax_elements">
    <div class="yellow-small-circle circle" data-relative-input="true" id="scene1c">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
    </div>
    <div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2c">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle"  data-relative-input="true" id="scene3c">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/small-blue-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>