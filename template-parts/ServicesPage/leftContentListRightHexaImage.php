<section class="leftContentListRightHexaImage Section">
  <div class="container">
    <div class="innerContainer">
      <div class="row alignItemCenter">
        <div class="col-lg-6 order-2-lg">
          <h1>Innovative areas of collaboration</h1>
          <p>While Bizspoke’s core strength lies in conceptualizing and delivering top-class events for its clients, the company’s management is not losing sight of new, exciting areas to venture into, such as:</p>
          <ul class="iconList mt-3">
            <li>
              <img class="icon" src="assets/img/services-page/ip-creation.svg" alt="ip-creation">
              <span><strong>IP Creation - </strong> Collaboratively developing original Bizspoke IPs that allow the brand to grow into dedicated B2C audiences</span>
            </li>
            <li>
              <img class="icon" src="assets/img/services-page/e-learning.svg" alt="ip-creation">
              <span><strong>E-learning Platform - </strong>  Funnelling international courses, classes, and experiences provided by partner websites and other experts through Bizspoke in India for its corporate clients</span>
            </li>
            <li>
              <img class="icon" src="assets/img/services-page/branded-content.svg" alt="ip-creation">
              <span><strong>Branded Content Partnerships - </strong>  Designing dedicated content plug-ins for like-minded and non-competitive third-party brands as part of Bizspoke’s experiences</span>
            </li>
            <li>
              <img class="icon" src="assets/img/services-page/gamification.svg" alt="ip-creation">
              <span><strong>Gamification - </strong>  Through playfully inspiring teams to do great work, HR departments can now work with Bizspoke on gamification formats to increase employee engagement</span>
            </li>
            <li>
              <img class="icon" src="assets/img/services-page/talent-management.svg" alt="ip-creation">
              <span><strong>Talent Management - </strong> Coaching and talent management services to be integrated in Bizspoke’s client experiences and events</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-6 mb-3-md">
          <div class="hexagonImageMain">
            <img src="assets/img/services-page/service-Innovative.png" alt="hexa_image">
          </div>
          <!-- <div class="hexagonImageWrap">
            <div class="hexagonImage">
               <img src="assets/img/hexa_image.png" alt="hexa_image">
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- Parallax Image -->
  <div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1b">
        <img src="assets/img/yellow-small-circle.png" alt="image">
      </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2b">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/yellow-medium-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle" id="scene3b">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/small-blue-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>