<section class="topImageBottomContent caseStudies Section">
	<div class="mainHeading">
		<div class="container">
			<h1>Case Studies</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas, tellus ut sollicitudin tristique, mi felis feugiat nisl, ac tristique est mi non mi.</p>
		</div>
	</div>
	<div class="threeCardsWithElements">
		<div class="container">
			<div class="row justifyCenter">
				<div class="col-md-4 col-sm-6 mb-3-md">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/jeremy-mcgilvrey.png" alt="event_image">
						</div>
						<div class="content">
							<h3>IIFL Founder’s Birthday Celebration.</h3>
							<p>Comprising of seasoned professionals with diverse experience in media</p>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 mb-3-md">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/reimond-de-zuniga.png" alt="event_image">
						</div>
						<div class="content">
							<h3>Everstone Group Employee Engagement</h3>
							<p>Comprising of seasoned professionals with diverse experience in media</p>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 mb-3-md">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/pexels-matheus.png" alt="event_image">
						</div>
						<div class="content">
							<h3>IIFL WAM Employee Engagement</h3>
							<p>Comprising of seasoned professionals with diverse experience in media</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		 <div class="parallax_elements">
		    <div class="circle" id="scene2">
		      <div class="imgWrap" data-depth="0.9">
		      <img src="assets/img/yellow-medium-circle.png" alt="image">
		        </div>
		    </div>
		 </div>
	</div>
    <div class="SingleEventCard">
    	<div class="container">
			<div class="row">
				<div class="col-12 mb-3-md mt-3">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/iifl-birth-celebration.png" alt="event_image">
						</div>
						<div class="content">
							<h3>IIFL Founder’s Birthday Celebration.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas, tellus ut sollicitudin tristique, mi felis feugiat nisl, ac tristique est mi non mi. Proin a aliquet lectus. In ac eros quis sapien pulvinar sodales congue in mi. Sed tristique, ipsum ut euismod vulputate, urna lorem porttitor odio, et dignissim odio erat accumsan velit.</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="parallax_elements">
		    <div class="circle">
		      <div class="imgWrap" data-depth="0.9" id="scene1">
		        <img src="assets/img/yellow-small-circle.png" alt="image">
		      </div>
		    </div>
	    </div>
	</div>
		<div class="twoEventCards">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 mb-3-md mt-3">
						<a href="#" class="EventCardWrap">
							<div class="imgWrap mb-2">
								<img src="assets/img/events-images/iifl-events-engagement.png" alt="event_image">
							</div>
							<div class="content">
								<h3>Everstone Group Employee Engagement</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas, tellus ut sollicitudin tristique, mi felis feugiat nisl, ac tristique est mi non mi.</p>
							</div>
						</a>
					</div>
					<div class="col-sm-6 mb-3-md mt-3">
						<a href="#" class="EventCardWrap">
							<div class="imgWrap mb-2">
								<img src="assets/img/events-images/iifl-events-engagement.png" alt="event_image">
							</div>
							<div class="content">
								<h3>Everstone Group Employee Engagement</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas, tellus ut sollicitudin tristique, mi felis feugiat nisl, ac tristique est mi non mi.</p>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="parallax_elements">
		    <div class="circle" id="scene2">
		      <div class="imgWrap" data-depth="0.9">
		      <img src="assets/img/yellow-medium-circle.png" alt="image">
		        </div>
		    </div>
		 </div>
		</div>

	<div class="threeCardsWithElements">
		<div class="container">
			<div class="row justifyCenter">
				<div class="col-md-4 col-sm-6 mb-3-md">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/jeremy-mcgilvrey.png" alt="event_image">
						</div>
						<div class="content">
							<h3>IIFL Founder’s Birthday Celebration.</h3>
							<p>Comprising of seasoned professionals with diverse experience in media</p>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 mb-3-md">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/reimond-de-zuniga.png" alt="event_image">
						</div>
						<div class="content">
							<h3>Everstone Group Employee Engagement</h3>
							<p>Comprising of seasoned professionals with diverse experience in media</p>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 mb-3-md">
					<a href="#" class="EventCardWrap">
						<div class="imgWrap mb-2">
							<img src="assets/img/events-images/pexels-matheus.png" alt="event_image">
						</div>
						<div class="content">
							<h3>IIFL WAM Employee Engagement</h3>
							<p>Comprising of seasoned professionals with diverse experience in media</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="parallax_elements">
		    <div class="circle" id="scene2">
		      <div class="imgWrap" data-depth="0.9">
		      <img src="assets/img/yellow-medium-circle.png" alt="image">
		        </div>
		    </div>
		</div>
	</div>
	<div class="btnWrap text-center">
		<a href="" class="Btn transparent">Load More</a>
	</div>
</section>