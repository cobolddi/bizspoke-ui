<section class="caseStudyCardSection Section caseStudiesPage">
	<div class="mainHeading">
		<div class="container">
			<h1>Case Studies</h1>
			<p>Sometimes, pictures say more than a 1000 words! Browse below through our latest event case studies.</p>
		</div>
	</div>
	<div class="container">
		<div class="row justifyCenter">
			<!-- Card 1 -->
			<div class="col-md-4 col-sm-6 mb-3">
				<a href="single-case-study.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/iifl-image-screenshot.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>IIFL WAM Surge</h3>
						<p>During the midst of the lockdown, IIFL decided to host a fun evening to motivate its team member</p>
					</div>
				</a>
			</div>
			<!-- Card 2 -->
			<div class="col-md-4 col-sm-6 mb-3">
				<a href="chapter-opening-ceremony.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/reimond-de-zuniga.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<!-- <img src="assets/img/tempimage/iifl-logo-small.png" alt="logo"> -->
							<span class="text">Corporate Client</span>
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>Chapter Opening Ceremony</h3>
						<p>To welcome its new members and bid farewell to outgoing associates, Corporate Client…</p>
					</div>
				</a>
			</div>
			<!-- Card 3 -->
			<div class="col-md-4 col-sm-6 mb-3">
				<a href="iifl-wealth-masterclass-with-gary-mehigan.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/iifl-image-4c.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>IIFL Wealth Masterclass with Gary Mehigan</h3>
						<p>Aiming to offer its clients a one-of-a-kind-experience, IIFL Wealth tasked Bizspoke to create a hybrid event…</p>
					</div>
				</a>
			</div>
      <!-- Card 4 -->
			<div class="col-md-12  mb-3">
				<a href="mixology-masterclass-with-macarena-rotger.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/iifl-image-4c.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<!-- <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo"> -->
              <span class="text">Corporate Client</span>
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>Mixology Masterclass with Macarena Rotger</h3>
						<p>Combining the best of the virtual and the real world, Bizspoke designed and delivered a unique mixology masterclass for all members of the client. A stylish hamper containing cocktail ingredients for cocktails such as ‘The Godfather’, ‘Blown Away’, and ‘Porn Star Martini’, were sent across to each member’s residence, with an invite to log in for a digital mixology masterclass with the Chilean celebrity bartender, Macarena Rotger. Cheers!</p>
					</div>
				</a>
			</div>
      <!-- Card 5 -->
			<div class="col-md-6 col-sm-6 mb-3">
				<a href="everstone-social-series.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/everstone-social-series.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>Everstone Social Series</h3>
						<p>As a part of the client’s employee engagement activities, Bizspoke planned and implemented two events for the Mumbai-based Everstone Group.</p>
					</div>
				</a>
			</div>
      <!-- Card 6 -->
			<div class="col-md-6 col-sm-6 mb-3">
				<a href="fireside-chat-series.php" class="caseStudyCard">
					<div class="caseStudyCard_imageWrap">
						<img src="assets/img/tempimage/fireside-chat-series.png" alt="event_image" class="grayScale">
					</div>
					<div class="caseStudyCard_content">
						<div class="leftlogoRightDate mb-2">
							<img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
							<small class='grayText'>20 March 2020</small>
						</div>
						<h3>Fireside Chat Series</h3>
						<p>Fireside Chats were conceptualized and executed by Bizspoke to digitally connect India’s industry leaders and experts with YPO Bombay members.</p>
					</div>
				</a>
			</div>
		</div>
    <!-- <div class="btnWrap text-center">
			<a href="#" class="Btn transparent">Load More</a>
		</div> -->
	</div>
	<!-- Parallax -->
	<div class="parallax_elements">
		<div class="yellow-small-circle circle" data-relative-input="true" id="scene1">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/yellow-small-circle.png" alt="image">
		  </div>
		</div>
		<div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/yellow-medium-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
		</div>
		<div class="blue-small-circle circle"  data-relative-input="true" id="scene3">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/small-blue-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		</div>
		<div class="blue-big-circle circle"  data-relative-input="true" id="scene4">
			<div class="imgWrap" data-depth="0.6">
			  <img src="assets/img/small-blue-circle.png" alt="image">
		  </div>
			<!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
		</div>
	</div>
</section>