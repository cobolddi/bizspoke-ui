<section class="footerTop">
  <div class="backgroundHexa"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2>We are excited to hear about your ideas!</h2>
      </div>
      <div class="col-md-4">
        <div class="btnWrap">
          <a href="contact-us.php" class="Btn whiteBg">Contact us</a>
        </div>
      </div>
    </div>
  </div>
</section>