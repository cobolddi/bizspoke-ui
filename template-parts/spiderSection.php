<section class="ourStrength Section">
	<div class="container">
		<div class="headContent">
			<h2>What we stand for</h2>
			<p>A simple hexagon is the most stable & efficient shape in all of geometry. It has over 95% packing ratio, it has the strength of a triangle and it can be efficiently repeated forever. We believe we are like that. The Hexagon. A reliable, repeatable, efficient partner for all you events. Here are the six values we inspire in all our team members.</p>
		</div>
	  <div class="spiderSection">
      <div class="InnerBox">
        <div class="row">
          <div class="col-lg-4 col-6">
            <ul>
              <li>
                <div class="iconWrap">
                  <img src="assets/img/icons/check-mark.svg" alt="Icon">
                </div>
                <span>Get it done attitude!</span>	 
              </li>
              <li>        
                <div class="iconWrap">
                  <img src="assets/img/icons/brain.svg" alt="Icon">
                </div>
                <span>Mindful Presence</span>
              </li>
              <li>    
                <div class="iconWrap">
                  <img src="assets/img/icons/speed.svg" alt="Icon">
                </div>
                <span>Perception & Action</span>
              </li>
            </ul>
          </div>  

          <div class="col-lg-4 text-center lg-3-md">
            <div class="imgWrap">
              <img src="assets/img/b-white-hexagon-b-logo.svg" alt="b-image">
            </div>
          </div>

          <div class="col-lg-4 col-6">
            <ul>
              <li>
                <div class="iconWrap">
                  <img src="assets/img/icons/scale.svg" alt="Icon">
                </div>
                <span>Transparency</span> 
              </li>
              <li> 
                <div class="iconWrap">
                  <img src="assets/img/icons/lightbulb.svg" alt="Icon">
                </div>
                  <span>Creativity</span>  
              </li>
              <li>    
                <div class="iconWrap">
                  <img src="assets/img/icons/solution.svg" alt="Icon">
                </div>
                <span>Continuous Learning</span>  
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
	</div>
</section>