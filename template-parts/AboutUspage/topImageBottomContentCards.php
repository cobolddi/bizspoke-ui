
<section class="topImageBottomContent Section">
  <div class="container">
    <div class="row justifyCenter">
      <div class="col-md-4 col-sm-6 mb-3-md">
        <a href="#" class="EventCardWrap">
          <div class="imgWrap mb-2">
            <img src="assets/img/events-images/corporate-events.png" alt="event_image">
          </div>
          <div class="content">
            <h3>Corporate Events</h3>
            <ul>
              <li>Employee Engagement – Team Building Events </li>
              <li>Annual Day / Family Day</li>
              <li>Rewards & Recognitions Night</li>
            </ul>
          </div>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 mb-3-md">
        <a href="#" class="EventCardWrap">
          <div class="imgWrap mb-2">
            <img src="assets/img/events-images/celebrations.png" alt="event_image">
          </div>
          <div class="content">
            <h3>Celebrations</h3>
            <ul>
              <li>Anniversary / Birthday</li>
              <li>Cocktail Parties</li>
              <li>Weddings</li>
            </ul>
          </div>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 mb-3-md">
        <a href="#" class="EventCardWrap">
          <div class="imgWrap mb-2">
            <img src="assets/img/events-images/sports-events.png" alt="event_image">
          </div>
          <div class="content">
            <h3>Sports Events</h3>
            <ul>
              <li>League Development</li>
              <li>Venue Production / Stadium Management</li>
              <li>Franchise Management</li>
            </ul>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>