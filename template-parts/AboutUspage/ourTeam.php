<section class="ourTeam Section">
  <div class="container">
    <div class="headContent">
      <h2>Our Team</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam consectetur ut lorem in ornare. Etiam id orci cursus, interdum lectus ultricies, molestie erat.</p>
    </div>
    <div class="row">
      <div class="col-md-4 col-6">
        <a href="#" class="text-center">
          <div class="hexagonImage">
            <img src="assets/img/our_team.png" alt="team_memeber">
          </div>
          <h3>Person Name</h3>
          <span>Designation</span>
        </a>
      </div>
      <div class="col-md-4 col-6">
        <a href="#" class="text-center">
          <div class="hexagonImage">
            <img src="assets/img/our_team.png" alt="team_memeber">
          </div>
          <h3>Person Name</h3>
          <span>Designation</span>
        </a>
      </div>
      <div class="col-md-4 col-6">
        <a href="#" class="text-center">
          <div class="hexagonImage">
            <img src="assets/img/our_team.png" alt="team_memeber">
          </div>
          <h3>Person Name</h3>
          <span>Designation</span>
        </a>
      </div>
    </div>
  </div>
</section>