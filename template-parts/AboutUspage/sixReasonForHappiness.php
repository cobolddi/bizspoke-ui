<section class="ourStrength Section">
	<div class="container">
    <h2 class="textCenter">6 Reasons for Bizspoke’s happiness</h2>
    <div class="spiderSection">
      <div class="InnerBox">
        <div class="spider">
          <ul class="spider_leg leftLeg">
            <li>
              <span>Transparency in the overall communicating and planning processes</span>	
              <div class="iconWrap">
                <img src="assets/img/icons/blue-eye.svg" alt="Icon">
              </div> 
            </li>
            <li>      
              <span>High creative output and out-of-the-box thinking at all stages of the project</span>  
              <div class="iconWrap">
                <img src="assets/img/icons/blue-brain.svg" alt="Icon">
              </div>
            </li>
            <li>
              <span>Respect for timelines, project milestones, and agreed budgets</span>
              <div class="iconWrap">
                <img src="assets/img/icons/blue-timeline.svg" alt="Icon">
              </div>
            </li>
          </ul>
          <div class="spider_body imgWrap">
            <img src="assets/img/blue-hexagonal.svg" alt="b-image">
          </div>
          <ul class="spider_leg rightLeg">
            <li>
              <div class="iconWrap">
                <img src="assets/img/icons/blue-fun.svg" alt="Icon">
              </div>
              <span>A fun, positive  attitude that always focuses on the solution, not the problem</span> 
            </li>
            <li>
              <div class="iconWrap">
                <img src="assets/img/icons/blue-lightbulb.svg" alt="Icon">
              </div>
              <span>Creating memories through documenting each event whit photos and videos</span> 
            </li>
            <li>
              <div class="iconWrap">
                <img src="assets/img/icons/blue-group.svg" alt="Icon">
              </div>
              <span>A highly experienced and motivated team brimming with creative ideas and joy for the job</span>  
            </li>
          </ul>
        </div>
        
        <!-- <div class="row">
          <div class="col-lg-4 col-6">
            <ul class="spiderSection_list">
              <li>
                <span>Transparency in the overall communicating and planning processes</span>	
                <div class="iconWrap">
                  <img src="assets/img/icons/blue-eye.svg" alt="Icon">
                </div> 
              </li>
              <li>      
                <span>High creative output and out-of-the-box thinking at all stages of the project</span>  
                <div class="iconWrap">
                  <img src="assets/img/icons/blue-brain.svg" alt="Icon">
                </div>
              </li>
              <li>
                <span>Respect for timelines, project milestones, and agreed budgets</span>
                <div class="iconWrap">
                  <img src="assets/img/icons/blue-timeline.svg" alt="Icon">
                </div>
              </li>
            </ul>
          </div>  

          <div class="col-lg-4 text-center lg-3-md">
            <div class="imgWrap">
              <img src="assets/img/blue-hexagonal.svg" alt="b-image">
            </div>
          </div>

          <div class="col-lg-4 col-6">
            <ul class="spiderSection_list">
              <li>
                <span>A fun, positive  attitude that always focuses on the solution, not the problem</span> 
                <div class="iconWrap">
                  <img src="assets/img/icons/blue-fun.svg" alt="Icon">
                </div>
              </li>
              <li>
                  <span>Creating memories through documenting each event whit photos and videos</span> 
                <div class="iconWrap">
                  <img src="assets/img/icons/blue-lightbulb.svg" alt="Icon">
                </div> 
              </li>
              <li>
                <span>A highly experienced and motivated team brimming with creative ideas and joy for the job</span>  
                <div class="iconWrap">
                  <img src="assets/img/icons/blue-group.svg" alt="Icon">
                </div>
              </li>
            </ul>
          </div>
        </div> -->
      </div>
    </div>
    <div class="headContent smallHeadContent">
			<h2>The Bizspoke Result:</h2>
			<p class="bigFont">We deliver great events and experiences within budgets, with joy.</p>
		</div>
	  <!-- <div class="WhiteBoxWrapper">
      <div class="InnerBox">
        <div class="row">
          <div class="col-lg-4 col-6">
            <ul>
              <li>
                <div class="iconWrap">
                  <img src="assets/img/icons/check-mark.svg" alt="Icon">
                </div>
                  <span>Get it done attitude!</span>	 
              </li>
              <li>        
                <div class="iconWrap">
                  <img src="assets/img/icons/brain.svg" alt="Icon">
                </div>
                <span>Mindful Presence</span>
              </li>
              <li>    
                <div class="iconWrap">
                  <img src="assets/img/icons/speed.svg" alt="Icon">
                </div>
                <span>Perception & Action</span>
              </li>
            </ul>
          </div>  
          <div class="col-lg-4 text-center lg-3-md">
            <div class="imgWrap">
              <img src="assets/img/b-white-hexagon-b-logo.svg" alt="b-image">
            </div>
          </div>
          <div class="col-lg-4 col-6">
            <ul>
              <li>
                <div class="iconWrap">
                  <img src="assets/img/icons/scale.svg" alt="Icon">
                </div>
                <span>Transparency</span> 
              </li>
              <li> 
                <div class="iconWrap">
                  <img src="assets/img/icons/lightbulb.svg" alt="Icon">
                </div>
                <span>Creativity</span>  
              </li>
              <li>    
                <div class="iconWrap">
                  <img src="assets/img/icons/solution.svg" alt="Icon">
                </div>
                <span>Continuous Learning</span>  
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div> -->
	</div>
  <!-- Parallax Element -->
  <div class="parallax_elements">
    <div class="yellow-small-circle circle" data-relative-input="true" id="scene1c">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
    </div>
    <div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2c">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle"  data-relative-input="true" id="scene3c">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/small-blue-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>