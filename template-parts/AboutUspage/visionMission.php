<section class="vision_mission Section">
  <div class="hexagonImageMain">
    <img src="assets/img/hexa-stem-list.png" alt="hexa_image">
  </div>
  <div class="container">
    <h2 class="textLeft">The Bizspoke Vision & Mission</h2>
    <div class="row">
      <div class="col-md-9">
        <div class="points">
          <div class="pointWrap">
            <div class="hexagonImage">
              <img src="assets/img/vision.svg" alt="image">
              <h3>Vision</h3>
              <p>What do we want to achieve in the future?</p>
            </div>
            <div class="description">
              <div class="content">
              <strong>To transform.</strong> <span>Bizspoke will develop its current core competency from a B2B event service sector into a platformdriven, IP-creating B2C experiential business model for India and abroad</span> 
              </div> 
            </div>
          </div>

          <div class="pointWrap">
            <div class="hexagonImage">
              <img src="assets/img/mission.svg" alt="image">
              <h3>Mission</h3>
              <p>How do we implement the vision?</p>
            </div>
            <div class="description">
              <div class="content">
              <strong>To elevate.</strong> <span> Bizspoke aims to evolve from being a vendor to a long-term partner within its eco-system through its thoughtleadership, diligent work ethic, quality, and its vision of pushing experiential boundaries for its customers</span> 
              </div>
            </div>
          </div>

          <div class="pointWrap">
            <div class="hexagonImage">
              <img src="assets/img/Emotional_Connect.svg" alt="image">
              <h3>Emotional Connect</h3>
              <p>What is our unique approach?</p>
            </div>
            <div class="description">
              <div class="content">
              <strong>To inspire.</strong> <span> Bizspoke connects to its customers, team members, and partners through trust, empathy, curiosity, and the unshakable belief that it can be relied upon.</span> <strong> We’ve got this!</strong> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3"></div>
    </div>
    <!-- Parallax Element -->
    <div class="parallax_elements">
      <div class="yellow-small-circle circle" data-relative-input="true" id="scene1b">
        <div class="imgWrap" data-depth="0.6">
        <img src="assets/img/yellow-small-circle.png" alt="image">
          </div>
      </div>
      <div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2b">
        <div class="imgWrap" data-depth="0.6">
        <img src="assets/img/yellow-medium-circle.png" alt="image">
          </div>
        <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
      </div>
      <div class="blue-small-circle circle"  data-relative-input="true" id="scene3b">
        <div class="imgWrap" data-depth="0.6">
        <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
          </div>
        <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
      </div>
    </div>
  </div>
</section>