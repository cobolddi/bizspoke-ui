<section class="leftImageRightContent aboutUsSection">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 px-0-sm">
				<div class="hexagonImageWrap">
           <div class="hexagonImage">
           	<img src="assets/img/tempimage/abput-stem-list-home.png" alt="hexa_image">
           </div>
				</div>
			</div>
			<div class="col-lg-7 col-md-6">
				<h2>We take being happy very seriously!</h2>
				<p>For Bizspoke, it is not only the result that matters; the process is also of equal importance. From the start to the finish of each project, we aim to create a positive work atmosphere with our clients, turning discussions about look, feel, flow, and cost of an event into a joyful Bizspoke experience.</p>
			</div>
		</div>
	</div>
  <div class="parallax_elements">
    <div class="yellow-small-circle circle" data-relative-input="true" id="scene1">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
    </div>
    <div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/yellow-medium-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle"  data-relative-input="true" id="scene3">
      <div class="imgWrap" data-depth="0.6">
      <img src="assets/img/small-blue-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>