<section class="testimonials text-center Section">
  <div class="container">
    <h2 class="mb-0">Testimonials</h2>
    <div class="testimonialSlider">
      <div class="testimonialSlide">
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id risus lectus. Duis sodales tristique mauris rhoncus posuere. Sed ac bibendum erat. Aliquam tempor mauris eu sapien tincidunt, nec tristique justo tincidunt. Proin bibendum libero fringilla lacinia faucibus.</p>
        </div>
        <div class="profile">
          <img src="assets/img/testimonial_image.png" alt="testimonial_image">
          <h3>Puneet Anand</h3>
          <span>company Name</span>
        </div>
      </div>
      <div class="testimonialSlide">
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id risus lectus. Duis sodales tristique mauris rhoncus posuere. Sed ac bibendum erat. Aliquam tempor mauris eu sapien tincidunt, nec tristique justo tincidunt. Proin bibendum libero fringilla lacinia faucibus.</p>
        </div>
        <div class="profile">
          <img src="assets/img/testimonial_image.png" alt="testimonial_image">
          <h3>Puneet Anand</h3>
          <span>company Name</span>
        </div>
      </div>
      <div class="testimonialSlide">
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id risus lectus. Duis sodales tristique mauris rhoncus posuere. Sed ac bibendum erat. Aliquam tempor mauris eu sapien tincidunt, nec tristique justo tincidunt. Proin bibendum libero fringilla lacinia faucibus.</p>
        </div>
        <div class="profile">
          <img src="assets/img/testimonial_image.png" alt="testimonial_image">
          <h3>Puneet Anand</h3>
          <span>company Name</span>
        </div>
      </div>
    </div>
  </div>
</section>