<section class="leftImageRightContent singleServiceSport">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 px-0-sm">
				<div class="hexagonImageWrap">
					<div class="hexagonImage">
					<img src="assets/img/sport-event.jpg" alt="hexa_image">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<h2>More about sporting events</h2>
				<p>Bizspoke is a boutique company which is entrenched in curating and creating immersive audience engagement platforms. Our core objective is to offer clients the opportunity to engage with their audiences/consumers in an enhanced way and deliver richer interactions through the Intellectual Properties (IP) we create.</p>
				<p> Comprising of seasoned professionals with diverse experience in media, event management and IP creation, the passionate team at Bizspoke is dedicated to delivering extraordinary experiences.</p>
			</div>
		</div>
	</div>
	 <div class="parallax_elements">
	    <div class="yellow-small-circle circle" data-relative-input="true" id="scene1">
	      <div class="imgWrap" data-depth="0.6">
	      <img src="assets/img/yellow-small-circle.png" alt="image">
	        </div>
	    </div>
	    <div class="yellow-medium-circle circle"  data-relative-input="true" id="scene2">
	      <div class="imgWrap" data-depth="0.6">
	      <img src="assets/img/yellow-medium-circle.png" alt="image">
	        </div>
	      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
	    </div>
	    <div class="blue-small-circle circle"  data-relative-input="true" id="scene3">
	      <div class="imgWrap" data-depth="0.6">
	      <img src="assets/img/small-blue-circle.png" alt="image">
	        </div>
	      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
	    </div>
	  </div>
</section>