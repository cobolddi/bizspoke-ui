<?php include('template-parts/header.php'); ?>

<section class="GalleryPage Section">
  <div class="galleryInner">
	  <div class="container">
      <div class="mainHeading">
        <h1>Gallery</h1>
        <p>Sometimes, pictures say more than a 1000 words! Browse below through our latest event case studies.</p>
      </div>
      <!-- <div class="filterNav">
        <ul class="button-group filters-button-group">
          <li><button class="button is-checked" data-filter="*">All Events</button></li>
          <li><button class="button" data-filter=".corporate_events">Corporate Events</button></li>
          <li><button class="button" data-filter=".celebration">Celebrations</button></li>
          <li><button class="button" data-filter=".sport_events">Sports Events</button></li>
        </ul>
      </div> -->
	
      <div class="MasonaryGallery grid row">
        <div class="col-12 col-md-4 ServicesItem element-item corporate_events"     data-category="CorporateEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img1.png"> 
              <img src="assets/img/gallery-images/gallery-img1.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item corporate_events" data-category="CorporateEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img2.png">
              <img src="assets/img/gallery-images/gallery-img2.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="Gcol-12 col-md-4 ServicesItem element-item corporate_events" data-category="CorporateEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img2.png">
              <img src="assets/img/gallery-images/gallery-img2.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="col-12 col-md-4 ServicesItem element-item celebration" data-category="Celebrations">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img4.png">
              <img src="assets/img/gallery-images/gallery-img4.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="col-12 col-md-4 ServicesItem element-item celebration" data-category="Celebrations">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img5.png">
              <img src="assets/img/gallery-images/gallery-img5.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="Gcol-12 col-md-4 ServicesItem element-item celebration" data-category="Celebrations">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img6.png">
              <img src="assets/img/gallery-images/gallery-img6.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item sport_events" data-category="SportsEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img7.png">
              <img src="assets/img/gallery-images/gallery-img7.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item sport_events" data-category="SportsEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img9.png">
              <img src="assets/img/gallery-images/gallery-img9.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item sport_events" data-category="SportsEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img10.png">
              <img src="assets/img/gallery-images/gallery-img10.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item corporate_events" data-category="CorporateEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img1.png"> 
              <img src="assets/img/gallery-images/gallery-img1.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item corporate_events" data-category="CorporateEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img2.png">
              <img src="assets/img/gallery-images/gallery-img2.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="Gcol-12 col-md-4 ServicesItem element-item corporate_events" data-category="CorporateEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img2.png">
              <img src="assets/img/gallery-images/gallery-img2.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="col-12 col-md-4 ServicesItem element-item celebration" data-category="Celebrations">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img4.png">
              <img src="assets/img/gallery-images/gallery-img4.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="col-12 col-md-4 ServicesItem element-item celebration" data-category="Celebrations">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img5.png">
              <img src="assets/img/gallery-images/gallery-img5.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="Gcol-12 col-md-4 ServicesItem element-item celebration" data-category="Celebrations">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img6.png">
              <img src="assets/img/gallery-images/gallery-img6.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item sport_events" data-category="SportsEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img7.png">
              <img src="assets/img/gallery-images/gallery-img7.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>

        <div class="col-12 col-md-4 ServicesItem element-item sport_events" data-category="SportsEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img9.png">
              <img src="assets/img/gallery-images/gallery-img9.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
        <div class="col-12 col-md-4 ServicesItem element-item sport_events" data-category="SportsEvents">
          <div class="CaseStudyImg img-hover-zoom">
            <a href="assets/img/gallery-images/gallery-img10.png">
              <img src="assets/img/gallery-images/gallery-img10.png" class="lazy" alt="gallery-img">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include('template-parts/footer.php'); ?>
