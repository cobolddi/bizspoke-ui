<?php include('template-parts/header.php'); ?>

<section class="GalleryPage Section gallery">
  <div class="background-light-hexa"></div>
  <div class="galleryInner"></div>
  <div class="container">
    <div class="mainHeading">
      <h1>Gallery</h1>
      <p>Sometimes, pictures say more than a 1000 words! Browse below through our latest event case studies.</p>
    </div>
    <div class="row zoom-gallery paginationGallery">
      <!-- First Page Images -->
      <div class="col-md-6 col-12">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image1.png">
          <img src="assets/img/gallery-images/gallery-image1.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-3 col-6">
        <div class="img-group">
          <div class="ImgWrap">
            <a href="assets/img/gallery-images/gallery-image2.png">
              <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
            </a>
          </div>
          <div class="ImgWrap">
            <a href="assets/img/gallery-images/gallery-image3.png">
              <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image4.png">
            <img src="assets/img/gallery-images/gallery-image4.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="img-group">
          <div class="ImgWrap">
            <a href="assets/img/gallery-images/gallery-image2.png">
              <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
            </a>
          </div>
          <div class="ImgWrap">
            <a href="assets/img/gallery-images/gallery-image3.png">
              <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image4.png">
            <img src="assets/img/gallery-images/gallery-image4.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="img-group">
          <div class="ImgWrap">
            <a href="assets/img/gallery-images/gallery-image2.png">
              <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
            </a>
          </div>
          <div class="ImgWrap">
            <a href="assets/img/gallery-images/gallery-image3.png">
              <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
            </a>
          </div>
        </div>
      </div> <!-- /First Page Images -->
      <!-- Second Page Images -->
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image3.png">
            <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image3.png">
            <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image3.png">
            <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image3.png">
            <img src="assets/img/gallery-images/gallery-image3.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <div class="col-md-4 col-6">
        <div class="ImgWrap">
          <a href="assets/img/gallery-images/gallery-image2.png">
            <img src="assets/img/gallery-images/gallery-image2.png" alt="image">
          </a>
        </div>
      </div>
      <!-- /Second Page Images -->
    </div>
    <div id="pagination-container"></div>
  </div>
  <!-- Parallax -->
  <div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1">
        <img src="assets/img/yellow-small-circle.png" alt="image">
      </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/yellow-medium-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
    <div class="blue-small-circle circle" id="scene3">
      <div class="imgWrap" data-depth="0.9">
        <img src="assets/img/small-blue-circle.png" alt="image">
      </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>


<?php include('template-parts/footer.php'); ?>
