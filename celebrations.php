<?php include('template-parts/header.php'); ?>


<?php// @include('template-parts/SingleEventPage/eventDescription.php'); ?>

<section class="eventPlanStart">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="contentWrap">
					<div class="mainHeading text-left">
						<h1>Joyous Occasions</h1>
						<p>Bizspoke loves to celebrate your special day with you! Our teams are committed to provide a stressfree, fun, creative, and full-service experience for its clients that allow them to enjoy their moment in the spotlight the most. Bizspoke is a leading tastemaker, with an in-depth understanding of the latest event trends, technology and design integration, and A-class entertainment</p>
					</div>
					<div class="btnWrap">
						<a href="contact-us.php" class="Btn transparent">GET STARTED</a>
					</div>
          <div class="listMain">
            <h3>We specialise in the following services for a broad variety of festive occasions:</h3>
            <div class="accordion">
              <div class="accordion_content">
                <div class="accordion_head blueText">Opening / Closing Ceremonies </div>
                <!-- <div class="accordion_body" style="display: none;">
                  <ul class="buletText">
                    <li>Venue Branding</li>
                    <li>Venue Operations</li>
                    <li>Venue OperationsSports</li>
                    <li>Operations Hospitality</li>
                  </ul>
                </div> -->
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Charity Gala Dinners</div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Weddings</div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Celebrations</div>
              </div>
            </div>
          </div>
					<!-- <ul>
						<li><a href="">SPORTS EVENT ORGANISATION</a></li>
						<li><a href="">PLANNING & EXECUTION</a></li>
						<li><a href="">STAGE & CEREMONY DESIGN</a></li>
						<li><a href="">TURN KEY SPORTS EVENTS</a></li>
					</ul> -->
				</div>
			</div>
			<div class="col-lg-7">
				<div class="b-shape-hexagon">
					<img src="assets/img/tempimage/sports-image-65.png" alt="image" class="ob-fit-cover">
				</div>
			</div>
		</div>
	</div>
  <!-- Parallax -->
	<div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2">
      <div class="imgWrap" data-depth="0.9">
      <img src="assets/img/yellow-medium-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>

<?php @include('template-parts/SingleEventPage/eventsCardSection.php'); ?>
<?php @include('template-parts/SingleEventPage/imageGallery.php'); ?>


<?php include('template-parts/footer.php'); ?>