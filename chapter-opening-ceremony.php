<?php include('template-parts/header.php'); ?>


<section class="singleCaseStudyPage Section">
	<div class="SmallContainer">
    <div class="caseStudyCard">
      <div class="caseStudyCard_imageWrap">
        <img src="assets/img/tempimage/reimond-de-zuniga.png" alt="case-study-image">
      </div>
      <div class="caseStudyCard_content">
        <div class="leftlogoRightDate mb-2">
          <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
          <small class='grayText'>20 March 2020</small>
        </div>
        <h3>Chapter Opening Ceremony for a Corporate Client</h3>
        <p>To welcome its new members and bid farewell to outgoing associates, YPO Bombay requested Bizspoke to create a unique and exciting digital induction ceremony at the height of the global pandemic via Zoom. Kicking off the celebrations with a big surprise, Bollywood Celebrity Katrina Kaif greeted and interacted with all members. Continuing with interactive games to make the new incoming members feel welcome, and get everybody into a good mood for the rest of the evening. An added performance by ‘Umber Jafri & Band’ made the event a sure highlight in each participant’s social calendar</p>
      </div>
    </div>
    <div class="lefContentRightImage mt-2">
      <div class="row">
        <div class="col-md-6 mb-2-md">
          <!-- <div class="para">
            <p><strong>Entertainment:</strong> Comedian Anshu Mor, singer B. Praak and DJ Sumit Sethi enlightened the evening from 6pm to 9pm from their respective residences, with Karishma Kotak hosting the event as Emcee.</p>
          </div> -->
          <h2>KPIs across event:</h2>
          <ul class="blueList bullets">
            <li>Total number of participants: 165+ (= total smiles created)</li>
            <li>Total engagement time: 3 Hours Event</li>
          </ul>
        </div>
        <div class="col-md-6">
          <!-- <div class="imgWrap">
            <img src="assets/img/tempimage/member-image-756.png" alt="image">
          </div> -->
        </div>
      </div>
    </div>
	</div>
</section>

 
<?php include('template-parts/footer.php'); ?>

