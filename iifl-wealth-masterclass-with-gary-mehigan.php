<?php include('template-parts/header.php'); ?>


<section class="singleCaseStudyPage Section">
	<div class="SmallContainer">
    <div class="caseStudyCard">
      <div class="caseStudyCard_imageWrap">
        <img src="assets/img/tempimage/iifl-image-4c.png" alt="event_image">
      </div>
      <div class="caseStudyCard_content">
        <div class="leftlogoRightDate mb-2">
          <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
          <small class='grayText'>20 March 2020</small>
        </div>
        <h3>IIFL Wealth – Masterclass with Gary Mehigan</h3>
        <p>Aiming to offer its clients a one-of-a-kindexperience, IIFL Wealth tasked Bizspoke to create a hybrid event that brings joy as well as educates at the same time. Thus, Bizspoke conceptualized and managed an entire experience around famous Australian Masterchef, Gary Mehigan, who curated a special menu ― an assorted mushroom risotto; fresh peas, spinach and basil soup; chocolate truffles ― for IIFL Wealth’s clients. The experience started by sending across a stylish hamper containing the recipe, ingredients, and IIFL-branded aprons to each client’s personal residence, which came to life the next day when Gary Mehigan conducted the masterclass live via Zoo</p>
      </div>
    </div>
    <div class="lefContentRightImage mt-2">
      <div class="row">
        <div class="col-md-6 mb-2-md">
          <!-- <div class="para">
            <p><strong>Entertainment:</strong> Comedian Anshu Mor, singer B. Praak and DJ Sumit Sethi enlightened the evening from 6pm to 9pm from their respective residences, with Karishma Kotak hosting the event as Emcee.</p>
          </div> -->
          <h2>KPIs across event:</h2>
          <ul class="blueList bullets">
            <li>Total number of participants: 750+ (= total smiles created)</li>
            <li>Numbers of locations hit: 18 cities simultaneously across India </li>
            <li>Total number of meals cooked: 500 + meals for a total of more than 2000 family members</li>
            <li>Total engagement time: 60 minutes of cooking time + 20 minutes of engagement</li>
          </ul>
        </div>
        <div class="col-md-6">
          <!-- <div class="imgWrap">
            <img src="assets/img/tempimage/member-image-756.png" alt="image">
          </div> -->
        </div>
      </div>
    </div>
	</div>
</section>

 
<?php include('template-parts/footer.php'); ?>

