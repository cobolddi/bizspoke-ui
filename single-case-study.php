<?php include('template-parts/header.php'); ?>


<section class="singleCaseStudyPage Section">
	<div class="SmallContainer">
    <div class="caseStudyCard">
      <div class="caseStudyCard_imageWrap">
        <img src="assets/img/tempimage/case-200a5.png" alt="event_image">
      </div>
      <div class="caseStudyCard_content">
        <div class="leftlogoRightDate mb-2">
          <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
          <small class='grayText'>20 March 2020</small>
        </div>
        <h3>IIFL WAM Surge (Virtual Event)</h3>
        <p>During the midst of the lockdown, IIFL decided to host a fun evening to motivate its team members, and create joy while working from home. Bizspoke designed and delivered a virtual offsite for over 3 hours, offering entertainment, fun awards, and video snippets of different teams, thus giving the senior management the unique opportunity to interact with its employees in a light, easy-going way.</p>
      </div>
    </div>
    <div class="lefContentRightImage mt-2">
      <div class="row">
        <div class="col-md-6 mb-2-md">
          <div class="para">
            <p><strong>Entertainment:</strong> Comedian Anshu Mor, singer B. Praak and DJ Sumit Sethi enlightened the evening from 6pm to 9pm from their respective residences, with Karishma Kotak hosting the event as Emcee.</p>
          </div>
          <h2>KPIs across event:</h2>
          <ul class="blueList bullets">
            <li>Total number of participants: 600+ (= total smiles created)</li>
            <li>Polls held for best dancers</li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="imgWrap">
            <img src="assets/img/tempimage/member-image-756.png" alt="image">
          </div>
        </div>
      </div>
    </div>
	</div>
</section>

 
<?php include('template-parts/footer.php'); ?>

