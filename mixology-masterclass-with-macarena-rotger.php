<?php include('template-parts/header.php'); ?>


<section class="singleCaseStudyPage Section">
	<div class="SmallContainer">
    <div class="caseStudyCard">
      <div class="caseStudyCard_imageWrap">
        <img src="assets/img/tempimage/iifl-image-4c.png" alt="event_image">
      </div>
      <div class="caseStudyCard_content">
        <div class="leftlogoRightDate mb-2">
          <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
          <small class='grayText'>20 March 2020</small>
        </div>
        <h3>Mixology Masterclass with Macarena Rotger</h3>
        <p>Combining the best of the virtual and the real world, Bizspoke designed and delivered a unique mixology masterclass for all members of the client. A stylish hamper containing cocktail ingredients for cocktails such as ‘The Godfather’, ‘Blown Away’, and ‘Porn Star Martini’, were sent across to each member’s residence, with an invite to log in for a digital mixology masterclass with the Chilean celebrity bartender, Macarena Rotger. Cheers! </p>
      </div>
    </div>
    <div class="lefContentRightImage mt-2">
      <div class="row">
        <div class="col-md-6 mb-2-md">
          <!-- <div class="para">
            <p><strong>Entertainment:</strong> Comedian Anshu Mor, singer B. Praak and DJ Sumit Sethi enlightened the evening from 6pm to 9pm from their respective residences, with Karishma Kotak hosting the event as Emcee.</p>
          </div> -->
          <h2>KPIs across event:</h2>
          <ul class="blueList bullets">
            <li>Total number of cocktails consumed: 320</li>
            <li>Total number of participants: 80+ (= total smiles created)</li>
          </ul>
        </div>
        <div class="col-md-6">
          <!-- <div class="imgWrap">
            <img src="assets/img/tempimage/member-image-756.png" alt="image">
          </div> -->
        </div>
      </div>
    </div>
	</div>
</section>

 
<?php include('template-parts/footer.php'); ?>

