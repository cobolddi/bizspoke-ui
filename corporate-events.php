<?php include('template-parts/header.php'); ?>


<?php// @include('template-parts/SingleEventPage/eventDescription.php'); ?>

<section class="eventPlanStart">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="contentWrap">
					<div class="mainHeading text-left">
						<h1>Corporate Events</h1>
						<p>Bizspoke creates premium experiences that bring art to life. Our teams provide strategic and creative thought-leadership - from ideation to execution - to leverage the power of corporate events. Bizspoke’s aim: Delivering highly professional events, achieving our client’s business goals, and creating unforgettable moments at the same time.</p>
					</div>
					<div class="btnWrap">
						<a href="contact-us.php" class="Btn transparent">GET STARTED</a>
					</div>
          <div class="listMain">
            <h3>We specialise in the following corporate event services:</h3>
            <div class="accordion">
              <div class="accordion_content">
                <div class="accordion_head blueText">Activations</div>
                <!-- <div class="accordion_body" style="display: none;">
                  <ul class="buletText">
                    <li>Venue Branding</li>
                    <li>Venue Operations</li>
                    <li>Venue OperationsSports</li>
                    <li>Operations Hospitality</li>
                  </ul>
                </div> -->
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">MICE</div>
                <!-- <div class="accordion_body" style="display: none;">
                  <ul class="buletText">
                    <li>Venue Branding</li>
                    <li>Venue Operations</li>
                    <li>Venue OperationsSports</li>
                    <li>Operations Hospitality</li>
                  </ul>
                </div> -->
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Conferences | Board Meetings</div>
                <!-- <div class="accordion_body" style="display: none;">
                  <ul class="buletText">
                    <li>Venue Branding</li>
                    <li>Venue Operations</li>
                    <li>Venue OperationsSports</li>
                    <li>Operations Hospitality</li>
                  </ul>
                </div> -->
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Rewards & Recognition Nights</div>
              </div>
              <div class="accordion_content">
                <div class="accordion_head blueText">Product Launch</div>
              </div> 
              <div class="accordion_content">
                <div class="accordion_head blueText">Networking Meets / Dinners</div>
              </div> 
              <div class="accordion_content">
                <div class="accordion_head blueText">Team Building Events </div>
              </div>            
              
            </div>
          </div>
					<!-- <ul>
						<li><a href="">SPORTS EVENT ORGANISATION</a></li>
						<li><a href="">PLANNING & EXECUTION</a></li>
						<li><a href="">STAGE & CEREMONY DESIGN</a></li>
						<li><a href="">TURN KEY SPORTS EVENTS</a></li>
					</ul> -->
				</div>
			</div>
			<div class="col-lg-7">
				<div class="b-shape-hexagon">
					<img src="assets/img/tempimage/sports-image-65.png" alt="image" class="ob-fit-cover">
				</div>
			</div>
		</div>
	</div>
  <!-- Parallax -->
	<div class="parallax_elements">
    <div class="yellow-small-circle circle">
      <div class="imgWrap" data-depth="0.9" id="scene1">
      <img src="assets/img/yellow-small-circle.png" alt="image">
        </div>
    </div>
    <div class="yellow-medium-circle circle" id="scene2">
      <div class="imgWrap" data-depth="0.9">
      <img src="assets/img/yellow-medium-circle.png" alt="image">
        </div>
      <!-- <img src="assets/img/yellow-medium-circle.png" alt="image"> -->
    </div>
      <!-- <img src="assets/img/small-blue-circle.png" alt="image"> -->
    </div>
  </div>
</section>

<?php @include('template-parts/SingleEventPage/eventsCardSection.php'); ?>
<?php @include('template-parts/SingleEventPage/imageGallery.php'); ?>


<?php include('template-parts/footer.php'); ?>