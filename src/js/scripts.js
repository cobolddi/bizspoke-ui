'use strict';
// var vivus = new Vivus('vivusIcon', {type: 'delayed', duration: 150});

// var logo = new Vivus('vivus_icon', {
//   type: 'async',
//   duration: 500,
//   animTimingFunction: Vivus.EASE,
//   start: 'autostart'
// });

gsap.registerPlugin(ScrollTrigger);


function initHeader() {
  var tl = gsap.timeline({
    scrollTrigger: {
      trigger: '.leftContentRightSlider',
      toggleActions: 'restart none none none',
      // start: "top center",
      // markers: true,
    },
  });

  tl.from('.title-Wrap', {
    ease: 'power4',
    y: '+=4vh',
    duration: 2,
    scrub: true,
    opacity: 0,
  })
    tl.from(
      '.inner_title',
      {
        y: 100,
        duration: 0.8,
        ease: 'power2',
        stagger: 0.3,
      },
      0.66
    )
    tl.from(
      '.hexagonSlider',
      {
        y: 50,
        opacity: 0,
        duration: 0.8,
        ease: 'power2',
      },
      0.8
    );  
}

function topImageBottomContent() {
  var tl1 = gsap.timeline({
    scrollTrigger: {
      trigger: '.topImageBottomContent',
      toggleActions: 'restart none none none',
      // start: "top center",
      // markers: true,
    },
});

  tl1.from('.anim_ul', {
    ease: 'power4',
    y: '+=4vh',
    duration: 2,
    scrub: true,
    opacity: 0,
  })
    tl1.from(
      '.anim_li',
      {
        y: 20,
        duration: 0.8,
        ease: 'power2',
        stagger: 0.3,
      },
      0.65
    );    
}

function initGsap() {
  gsap.set(main, { autoplay: 1 });
  initHeader();
  topImageBottomContent();
}

 // Pre loader function
function preLoader() {
  $('body').addClass('loading_out');
  setTimeout(function () {
    $('body').removeClass('loading');
    $('body').removeClass('loading_out');
  }, 5000);
}

$(window).on('load', function(){
    preLoader();
    initGsap();
});



// Fixed Header on scroll for Desktop
window.onscroll = function() {scrollFunction()};

function scrollFunction() { 
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("header").classList.add("stickyHeader");
  } else {
    document.getElementById("header").classList.remove("stickyHeader");
  }
}

function menuDropdoenFn(){
  $('.hasDropdown>a').click(function(e){
    e.preventDefault();
    console.log("Mew has dropdown")
    if($('.hasDropdown>ul').is(':visible')){
      $('.hasDropdown>ul').slideUp(300);
      $(this).removeClass('open');
      console.log("slide..")
    }
    if($(this).next("ul").is(':visible')){
      $(this).next("ul").slideUp(300);
      $(this).removeClass('open');
    } else {
      $(this).next("ul").slideDown(300);
      $(this).addClass('open');
    }
  })
}

function accordionFn(){
  $('.accordion_head').click(function(){
    if($('.accordion_body').is(':visible')){
      $('.accordion_body').slideUp(300);
      $('.plusminus').text('+');
    }
    if($(this).next(".accordion_body").is(':visible')){
      $(this).next(".accordion_body").slideUp(300);
      $(this).children('.plusminus').text('+');
    } else {
      $(this).next(".accordion_body").slideDown(300);
      $(this).children('.plusminus').text('-');
    }
  })
}

function imageGalleryPaginationFn(){
  var items = $(".paginationGallery .ImgWrap");
    var numItems = items.length;
    var perPage = 9;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&#8249;",
        nextText: "&#8250;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
}


$(document).ready(function(){
  var screenWidth = $(window).width();
  console.log('screenWidth: ', screenWidth);

  imageGalleryPaginationFn()
  accordionFn();
  menuDropdoenFn();

  // About us page 'Why Choose Bizspoke' blueBox
  var whyChooseBizspokeSection = $('.aboutUsPage.whyChooseBizspoke');
  var blueBox = $('.aboutUsPage.whyChooseBizspoke .blueBoxContent');
  var blueBoxHeight = $(blueBox).innerHeight();
  if(blueBoxHeight){
    console.log('blueBoxHeight: ', blueBoxHeight)
    // $(blueBox).css('height', blueBoxHeight);
    if(screenWidth >= 576){
      $(whyChooseBizspokeSection).css({
        marginBottom: blueBoxHeight/2,
        paddingBottom: (blueBoxHeight/2)+10,
      });
    }
  }
  

  $(".hexagonSlider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots:false, 
    arrow: false,
    fade: true,
    autoplay: true,
    pauseOnHover: false,
    speed: 800,
    infinite: true,
    cssEase: 'ease-in-out',
    touchThreshold: 100
  });


  // Testimonial slider
  $('.testimonialSlider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 800,
    autoplaySpeed: 2000,
    arrows: true,
    dots:true,
    autoplay: true,
    fade:true,
    cssEase: 'linear'
  });

    //Mobile menu
    $("#header .menuBar").click(function(){
      $(this).parents("#header").toggleClass("Active_Nav");
      $(this).toggleClass('active');
    });

     // init Isotope
      // var $grid = $('.grid').isotope({
      //   itemSelector: '.element-item',
      //   layoutMode: 'fitRows'
      // });

      // filter functions
      var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
          var number = $(this).find('.number').text();
          return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
          var name = $(this).find('.name').text();
          return name.match( /ium$/ );
        }
      };

      // bind filter button click
      $('.filters-button-group').on( 'click', 'button', function() {
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({ filter: filterValue });
      });
      // change is-checked class on buttons
      $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
          $buttonGroup.find('.is-checked').removeClass('is-checked');
          $( this ).addClass('is-checked');
        });
      });

      // Magnific Zoom Gallery
      $('.MasonaryGallery').magnificPopup({
          delegate: 'a',
          type: 'image',
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: 'mfp-with-zoom mfp-img-mobile',
          image: {
            verticalFit: true,
            titleSrc: function(item) {
              return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            }
          },
          gallery: {
            enabled: true
          },
          zoom: {
            enabled: true,
            duration: 300,
            opener: function(element) {
              return element.find('img');
            }
          }
          
        });


      var $grid = $(".MasonaryGallery").isotope({
        itemSelector: '.ServicesItem',
        layoutMode: 'masonry',
        percentPosition: true,
        stagger: 30,
        hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },
      });
  // // filter items on button click
  // $('.ServiceFilterButton').on( 'click', 'button', function() {
  //   var filterValue = $(this).attr('data-filter');
  //   console.log(filterValue);
  //   $grid.isotope({ filter: filterValue });
  //   $('.ServiceFilterButton button').removeClass('is_active');
  //   $(this).addClass('is_active');
  // });


  $('.zoom-gallery').magnificPopup({
      delegate: 'a',
      type: 'image',
      closeOnContentClick: false,
      closeBtnInside: false,
      mainClass: 'mfp-with-zoom mfp-img-mobile',
      image: {
        verticalFit: true,
        titleSrc: function(item) {
          return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
        }
      },
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: true,
        duration: 300,
        opener: function(element) {
          return element.find('img');
        }
      }
    
    });
});

function vivusjs(){
  var iconThinking = $("#icon-thinking");
  var iconDesign = $("#icon-design");
  var iconGear = $("#icon-gear");
  // var iconVirtual  = $("#icon-Virtual");
  // var iconPlanning = $("#icon-palnning");
  // var iconProduction = $("#iconProduction");
  // var iconEntertainment = $("#icon-entertainment")

  // if(iconVirtual){
  //   var icon_virtual = new Vivus('icon-Virtual', {
  //     type: 'async',
  //     duration: 100,
  //     animTimingFunction: Vivus.EASE,
  //     start: 'autostart'
  //   });
  // }

  // if(iconEntertainment){
  //   var icon_entertainment = new Vivus('icon-entertainment', {
  //     type: 'async',
  //     duration: 100,
  //     animTimingFunction: Vivus.EASE,
  //     start: 'autostart'
  //   });
  // }
  
  // if(iconPlanning){
  //   var icon_planning = new Vivus('icon-palnning', {
  //     type: 'async',
  //     duration: 100,
  //     animTimingFunction: Vivus.EASE,
  //     start: 'autostart'
  //   });
  // }
  // if(iconProduction){
  //   var icon_production = new Vivus('icon-production', {
  //     type: 'async',
  //     duration: 100,
  //     animTimingFunction: Vivus.EASE,
  //     start: 'autostart'
  //   });
  // }

  var icon_thinking = new Vivus('icon-thinking', {
    type: 'async',
    duration: 100,
    animTimingFunction: Vivus.EASE,
    start: 'autostart'
  });
  var icon_design = new Vivus('icon-design', {
    type: 'async',
    duration: 100,
    animTimingFunction: Vivus.EASE,
    start: 'autostart'
  });
  var icon_gear = new Vivus('icon-gear', {
    type: 'async',
    duration: 100,
    animTimingFunction: Vivus.EASE,
    start: 'autostart'
  });
  
  
}

// var scene = document.getElementById('scene');
// var parallaxInstance = new Parallax(scene);


var scene1 = document.getElementById('scene1');
if(scene1){
  var parallaxInstance1 = new Parallax(scene1, {
    relativeInput: true
  });
  parallaxInstance1.friction(0.2, 0.2);
}
var scene2 = document.getElementById('scene2');
if(scene2){
  var parallaxInstance2 = new Parallax(scene2, {
    relativeInput: true
  });
  parallaxInstance2.friction(0.2, 0.2);
}
var scene3 = document.getElementById('scene3');
if(scene3){
  var parallaxInstance3 = new Parallax(scene3, {
    relativeInput: true
  });
  parallaxInstance3.friction(0.2, 0.2);
}
var scene4 = document.getElementById('scene4');
if(scene4){
  var parallaxInstance4 = new Parallax(scene4, {
    relativeInput: true
  });
  parallaxInstance4.friction(0.2, 0.2);
}

var scene1b = document.getElementById('scene1b');
if(scene1b){
  var parallaxInstance1b = new Parallax(scene1b, {
    relativeInput: true
  });
  parallaxInstance1b.friction(0.2, 0.2);
}

var scene2b = document.getElementById('scene2b');
if(scene2b){
  var parallaxInstance2b = new Parallax(scene2b, {
    relativeInput: true
  });
  parallaxInstance2b.friction(0.2, 0.2);
}

var scene3b = document.getElementById('scene3b');
if(scene3b){
  var parallaxInstance3b = new Parallax(scene3b, {
    relativeInput: true
  });
  parallaxInstance3b.friction(0.2, 0.2);
}
var scene4b = document.getElementById('scene4b');
if(scene4b){
  var parallaxInstance4b = new Parallax(scene4b, {
    relativeInput: true
  });
  parallaxInstance4b.friction(0.2, 0.2);
}


var scene1c = document.getElementById('scene1c');
if(scene1c){
  var parallaxInstance1c = new Parallax(scene1c, {
    relativeInput: true
  });
  parallaxInstance1c.friction(0.2, 0.2);
}
var scene2c = document.getElementById('scene2c');
if(scene2c){
  var parallaxInstance2c = new Parallax(scene2c, {
    relativeInput: true
  });
  parallaxInstance2c.friction(0.2, 0.2);
}
var scene3c = document.getElementById('scene3c');
if(scene3c){
  var parallaxInstance3c = new Parallax(scene3c, {
    relativeInput: true
  });
  parallaxInstance3c.friction(0.2, 0.2);
}
var scene4c = document.getElementById('scene4c');
if(scene4c){
  var parallaxInstance4c = new Parallax(scene4c, {
    relativeInput: true
  });
  parallaxInstance4c.friction(0.2, 0.2);
}


var vivusAnimation = document.getElementById('vivusAnimatedIcon')
if(vivusAnimation){
  var waypoint = new Waypoint({
    element: vivusAnimation,
    handler: function() {
     vivusjs();
     // alert("hello");
    },
      offset: '75%'
  })
}



// new Vivus('vivusIcon', {type: 'scenario-sync', duration: 20, start: 'autostart', dashGap: 20, forceRender: false},
// function () {
//   if (window.console) {
//     console.log('Animation finished. [log triggered from callback]');
//   }
// }),
