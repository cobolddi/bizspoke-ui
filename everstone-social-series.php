<?php include('template-parts/header.php'); ?>


<section class="singleCaseStudyPage Section">
	<div class="SmallContainer">
    <div class="caseStudyCard">
      <div class="caseStudyCard_imageWrap">
        <img src="assets/img/tempimage/everstone-social-series.png" alt="event_image">
      </div>
      <div class="caseStudyCard_content">
        <div class="leftlogoRightDate mb-2">
          <img class="image" src="assets/img/tempimage/iifl-logo-small.png" alt="logo">
          <small class='grayText'>20 March 2020</small>
        </div>
        <h3>Everstone Social Series</h3>
        <p>As a part of the client’s employee engagement activities, Bizspoke planned and implemented two events for the Mumbaibased Everstone Group. With a focus on giving the Everstone team members a good time, and a well-deserved break from their Workfrom-Home-routine, Bizspoke created the overall concept of the Everstone Social Series, and designed and executed the event. In addition, the Bizspoke team ideated all Emcee-based engagements. </p>
      </div>
    </div>
    <div class="lefContentRightImage mt-2">
      <div class="row">
        <div class="col-md-6 mb-2-md">
          <div class="para">
            <p><strong>Entertainment:</strong> Virtual performance of DJ Sunny playing Bollywood Music. <br>
            Games played such as “Never have I ever”, and “Hot Seat Roast” among others.
          </p>
          </div>
          <h2>KPIs across event:</h2>
          <ul class="blueList bullets">
            <li>Total number of participants: 120+ (= total smiles created)</li>
            <li>Total engagement time: 60 minutes</li>
            <li>High Employee Engagement</li>
          </ul>
        </div>
        <div class="col-md-6">
          <!-- <div class="imgWrap">
            <img src="assets/img/tempimage/member-image-756.png" alt="image">
          </div> -->
        </div>
      </div>
    </div>
	</div>
</section>

 
<?php include('template-parts/footer.php'); ?>

